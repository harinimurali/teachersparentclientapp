package com.school.teacherparent.models;

/**
 * Created by keerthana on 9/18/2018.
 */

public class NotiDTO {
    private String notiimg, img,title,time,desc;



    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }


    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }


    public String getNotiimg() {
        return notiimg;
    }

    public void setNotiimg(String notiimg) {
        this.notiimg = notiimg;
    }



    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }







}