package com.school.teacherparent.fragment;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


public class AddEventFragment extends Fragment {

    private LinearLayout lin_date;
    private TextView date, time, add_event;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private Spinner class_spinner;
    private Spinner status1;
    private LinearLayout lin_time;
    private int hour, minute;
    ImageView back;


    @RequiresApi(api = Build.VERSION_CODES.O)
    public AddEventFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_event, container, false);

        SidemenuDetailActivity.title.setText(getResources().getString(R.string.add_new_event));
        date = view.findViewById(R.id.date);
        lin_date = view.findViewById(R.id.lin_date);
        lin_time = view.findViewById(R.id.lin_time);
        add_event = view.findViewById(R.id.add_event);

        time = view.findViewById(R.id.time);
        String date_n = new SimpleDateFormat("dd MMM  yyyy", Locale.getDefault()).format(new Date());
        date.setText(date_n);
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("hh.mm a");
        String strDate = "" + mdformat.format(calendar.getTime());
        time.setText(strDate);

        add_event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Events Added Successfully", Toast.LENGTH_SHORT).show();
                getActivity().finish();
            }
        });

        lin_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view == lin_time) {

                    // Get Current Date
                    final Calendar c = Calendar.getInstance();
                    mHour = c.get(Calendar.HOUR_OF_DAY);
                    mMinute = c.get(Calendar.MINUTE);


                    TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),
                            new TimePickerDialog.OnTimeSetListener() {

                                public String format;

                                @Override
                                public void onTimeSet(TimePicker view, int hourOfDay,
                                                      int minute) {
                                    if (hourOfDay == 0) {

                                        hourOfDay += 12;

                                        format = "AM";
                                    } else if (hourOfDay == 12) {

                                        format = "PM";

                                    } else if (hourOfDay > 12) {

                                        hourOfDay -= 12;

                                        format = "PM";

                                    } else {

                                        format = "AM";
                                    }

                                    time.setText(hourOfDay + ":" + minute);
                                }
                            }, mHour, mMinute, false);
                    timePickerDialog.show();

                }
            }
        });


        lin_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view == lin_date) {

                    // Get Current Date
                    final Calendar c = Calendar.getInstance();
                    mYear = c.get(Calendar.YEAR);
                    mMonth = c.get(Calendar.MONTH);
                    mDay = c.get(Calendar.DAY_OF_MONTH);


                    DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                            new DatePickerDialog.OnDateSetListener() {


                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {
                                    SimpleDateFormat curFormater = new SimpleDateFormat("dd-MM-yyyy");
                                    Date dateObj = null;
                                    String a = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                                    try {
                                        dateObj = curFormater.parse(a);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    SimpleDateFormat formt = new SimpleDateFormat("dd MMM yyyy");

                                    date.setText(formt.format(dateObj));
                                }

                            }, mYear, mMonth, mDay);
                    datePickerDialog.show();
                }
            }
        });

        List<String> classpinner = new ArrayList<String>();
        classpinner.add("1 Classes");
        classpinner.add("2 Classes");
        classpinner.add("3 Classes");
        classpinner.add("4 Classes");
        classpinner.add("5 Classes");
        class_spinner = (Spinner) view.findViewById(R.id.class_spinner);

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, classpinner);

        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);

        class_spinner.setAdapter(spinnerArrayAdapter);

        class_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });
        Spinner spin = (Spinner) view.findViewById(R.id.status);
        ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("Name", "Public");
        map.put("Icon", R.mipmap.visiblity_icon);
        list.add(map);

        map = new HashMap<String, Object>();
        map.put("Name", "Private");
        map.put("Icon", R.mipmap.privacy_icon);
        list.add(map);


        myAdapter adapter = new myAdapter(getActivity(), list,
                R.layout.spinner_dropdown_item_image, new String[]{"Name", "Icon"},
                new int[]{R.id.name, R.id.icon});

        spin.setAdapter(adapter);
        return view;

    }

    private void display(String num) {
        TextView time = (TextView) getView().findViewById(R.id.time);
        time.setText(num);
    }

    private class myAdapter extends SimpleAdapter {

        public myAdapter(Context context, List<? extends Map<String, ?>> data,
                         int resource, String[] from, int[] to) {
            super(context, data, resource, from, to);

        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = getLayoutInflater().inflate(R.layout.spinner_dropdown_item_image,
                        null);
            }

            HashMap<String, Object> data = (HashMap<String, Object>) getItem(position);

            ((TextView) convertView.findViewById(R.id.name))
                    .setText((String) data.get("Name"));
            ((ImageView) convertView.findViewById(R.id.icon))
                    .setImageResource((Integer) data.get("Icon"));

            return convertView;
        }

    }
}
