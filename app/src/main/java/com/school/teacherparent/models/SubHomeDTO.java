package com.school.teacherparent.models;

/**
 * Created by keerthana on 9/27/2018.
 */


public class SubHomeDTO {
    private String img, title,subtitle,content,status1,subimg1,subimg2;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }



    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


    public String getSubimg1() {
        return subimg1;
    }

    public void setSubimg1(String subimg1) {
        this.subimg1 = subimg1;
    }


    public String getStatus1() {
        return status1;
    }

    public void setStatus1(String status1) {
        this.status1 = status1;
    }


    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getSubimg2() {
        return subimg2;
    }

    public void setSubimg2(String subimg2) {
        this.subimg2 = subimg2;
    }

}




