package com.school.teacherparent.utils;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by harini on 6/12/2018.
 */

public class FontTextViewLight extends android.support.v7.widget.AppCompatTextView {
    public FontTextViewLight(Context context) {
        super(context);
        setIncludeFontPadding(false);
        setTypeface(ProximaNovaFont.getInstance(context).getLightTypeFace());
    }

    public FontTextViewLight(Context context, AttributeSet attrs) {
        super(context, attrs);
        setIncludeFontPadding(false);
        setTypeface(ProximaNovaFont.getInstance(context).getLightTypeFace());
    }

    public FontTextViewLight(Context context, AttributeSet attrs,
                        int defStyle) {
        super(context, attrs, defStyle);
        setIncludeFontPadding(false);
        setTypeface(ProximaNovaFont.getInstance(context).getLightTypeFace());
    }
}
