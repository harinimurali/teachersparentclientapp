package com.school.teacherparent.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.school.teacherparent.adapter.SubHomeClassAdapter;
import com.school.teacherparent.models.SubHomeDTO;
import com.school.teacherparent.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by harini on 10/23/2018.
 */

public class HomeWorkClassWiseSubFragment extends Fragment {
    private RecyclerView recyclerView;
    private SubHomeClassAdapter mAdapter;
    private List<SubHomeDTO> subhomeList = new ArrayList<>();

    public HomeWorkClassWiseSubFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sub_category_home, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycle_sub_category_homefragment);
        Analdata();
        mAdapter = new SubHomeClassAdapter(subhomeList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);


       /* mAdapter.setOnClickListen(new SubHomeClassAdapter.AddTouchListen() {
            @Override
            public void OnTouchClick(int position) {
                Intent intent=new Intent(getActivity(), AddHomeWorkActivity.class);
                startActivity(intent);
            }
        });*/


        return view;

    }

    private void Analdata() {
        SubHomeDTO s = new SubHomeDTO();
        s.setTitle("English");
        s.setSubtitle("My mother at sixty two");
        s.setContent("Topic:2.4");
        s.setStatus1("Lorium ipsum dolar sit amet,constructor eclipsing elid, sed do elusmud");
        s.setImg(String.valueOf(R.mipmap.english_icon));
        s.setSubimg1(String.valueOf(R.drawable.blue_border_white_background));
        s.setSubimg2(String.valueOf(R.drawable.blue_border_white_background));
        subhomeList.add(s);


        s = new SubHomeDTO();
        s.setTitle("Science");
        s.setSubtitle("Anatomy of Human Beings");
        s.setContent("Topic:6.4");
        s.setStatus1("Human beings skeletal system and collections of Red Blood cells and White Blood cells");
        s.setImg(String.valueOf(R.mipmap.maths_icon));
        s.setSubimg1(String.valueOf(R.drawable.blue_border_white_background));
        s.setSubimg2(String.valueOf(R.drawable.blue_border_white_background));
        subhomeList.add(s);


        s = new SubHomeDTO();
        s.setTitle("Maths");
        s.setSubtitle("Trignomentry Formulae");
        s.setStatus1("Trignometry Formulae of sin,cos,tan theta and describe and solve pythagoras theorem");
        s.setContent("Topic:4");
        s.setImg(String.valueOf(R.mipmap.english_icon));
        s.setSubimg1(String.valueOf(R.drawable.blue_border_white_background));
        s.setSubimg2(String.valueOf(R.drawable.blue_border_white_background));
        subhomeList.add(s);


        s = new SubHomeDTO();
        s.setTitle("Social");
        s.setSubtitle("Indian Constitutional");
        s.setStatus1("Indian constitution of economy and welfare of Indian");
        s.setContent("Topic:8");
        s.setImg(String.valueOf(R.mipmap.maths_icon));
        s.setSubimg1(String.valueOf(R.drawable.blue_border_white_background));
        s.setSubimg2(String.valueOf(R.drawable.blue_border_white_background));
        subhomeList.add(s);

    }

}


