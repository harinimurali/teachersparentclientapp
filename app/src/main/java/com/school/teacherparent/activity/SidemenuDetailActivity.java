package com.school.teacherparent.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.school.teacherparent.fragment.AddAttendanceFragment;
import com.school.teacherparent.fragment.AddCircularFragment;
import com.school.teacherparent.fragment.AddClassWorkFragment;
import com.school.teacherparent.fragment.AddEventFragment;
import com.school.teacherparent.fragment.AddExamFragment;
import com.school.teacherparent.fragment.AddHomeWorkTabFragment;
import com.school.teacherparent.fragment.AddNewFeedFragment;
import com.school.teacherparent.fragment.AddResultFragment;
import com.school.teacherparent.fragment.FeedClapFragment;
import com.school.teacherparent.fragment.GalleryFragment;
import com.school.teacherparent.fragment.HelpFragment;
import com.school.teacherparent.fragment.SchoolFragment;
import com.school.teacherparent.fragment.SettingFragment;
import com.school.teacherparent.fragment.TimetableFragment;
import com.school.teacherparent.R;

/**
 * Created by harini on 10/12/2018.
 */

public class SidemenuDetailActivity extends AppCompatActivity {
    ImageView back;
    Fragment fragment;
    public static TextView title;


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sidemenu);


        Intent i = getIntent();
        if (i.hasExtra("type")) {
            String type = i.getStringExtra("type");
            switch (type) {
                case "school":
                    fragment = new SchoolFragment();
                    replaceFragment(fragment);
                    break;
                case "timetable":
                    fragment = new TimetableFragment();
                    replaceFragment(fragment);
                    break;
                case "gallery":
                    fragment = new GalleryFragment();
                    replaceFragment(fragment);
                    break;
                case "settings":
                    fragment = new SettingFragment();
                    replaceFragment(fragment);
                    break;
                case "help":
                    fragment = new HelpFragment();
                    replaceFragment(fragment);
                    break;
                case "attendance":
                    fragment = new AddAttendanceFragment();
                    replaceFragment(fragment);
                    break;
                case "circular":
                    fragment = new AddCircularFragment();
                    replaceFragment(fragment);
                    break;
                case "events":
                    fragment = new AddEventFragment();
                    replaceFragment(fragment);
                    break;
                case "exam":
                    fragment = new AddExamFragment();
                    replaceFragment(fragment);
                    break;
                case "result":
                    fragment = new AddResultFragment();
                    replaceFragment(fragment);
                    break;
                case "classwork":
                    fragment = new AddClassWorkFragment();
                    replaceFragment(fragment);
                    break;
                case "homework":
                    fragment = new AddHomeWorkTabFragment();
                    replaceFragment(fragment);
                    break;
                case "feed":
                    fragment = new AddNewFeedFragment();
                    replaceFragment(fragment);
                    break;
                case "claps":
                    fragment = new FeedClapFragment();
                    replaceFragment(fragment);
                    break;
            }
        }
        title = findViewById(R.id.title);
        back = findViewById(R.id.back);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    public void replaceFragment(Fragment fragment) {
        if (fragment != null) {
            android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
            android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.side_main, fragment, "");
            fragmentTransaction.commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_notification:
                startActivity(new Intent(SidemenuDetailActivity.this, NotificationActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
