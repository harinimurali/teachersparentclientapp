package com.school.teacherparent.fragment;



import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import android.widget.ImageView;


import com.school.teacherparent.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class LoginUserTypeFragment extends Fragment {


    private Button submit;
    private String techr = "0";
    private String parnt = "0";
    private ImageView checkTeacher, checkParent;

    public LoginUserTypeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login_usertype, container, false);

        checkTeacher = view.findViewById(R.id.check_parent);
        checkParent = view.findViewById(R.id.check_teacher);
        submit = view.findViewById(R.id.submit);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new LoginNumberFragment();
                replaceFragment(fragment);
            }
        });


        checkTeacher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (techr.equalsIgnoreCase("0")) {
                    checkTeacher.setImageResource(R.mipmap.tick_icon);//check
                    checkParent.setImageResource(R.mipmap.untick_icon);//unchek
                    techr = "1";
                    parnt = "0";

                } else {
                    checkTeacher.setImageResource(R.mipmap.untick_icon);//uncheck
                    checkParent.setImageResource(R.mipmap.tick_icon);//chek
                    techr = "0";
                    parnt = "1";

                }
            }
        });


        checkParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (parnt.equalsIgnoreCase("0")) {
                    checkParent.setImageResource(R.mipmap.tick_icon);//chek
                    checkTeacher.setImageResource(R.mipmap.untick_icon);//uncheck

                    parnt = "1";
                    techr = "0";

                } else {

                    checkParent.setImageResource(R.mipmap.untick_icon);//chek
                    checkTeacher.setImageResource(R.mipmap.tick_icon);//uncheck
                    parnt = "0";
                    techr = "1";

                }
            }
        });

        return view;
    }
    public void replaceFragment(Fragment fragment) {
        if (fragment != null) {
            android.support.v4.app.FragmentManager fragmentManager = getFragmentManager();
            android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.login_frame, fragment, "");
            fragmentTransaction.commit();
        }
    }
}
