package com.school.teacherparent.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.school.teacherparent.adapter.MyClassAdapter;
import com.school.teacherparent.models.MyClassDTO;
import com.school.teacherparent.R;

import java.util.ArrayList;
import java.util.List;



/**
 * A simple {@link Fragment} subclass.
 */
public class StudentFragment extends Fragment {


    private RecyclerView recyclerview;
    private MyClassAdapter mAdapter;
    private List<MyClassDTO> stuList=new ArrayList<>();

    public StudentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view=inflater.inflate(R.layout.fragment_student, container, false);

        recyclerview=(RecyclerView)view.findViewById(R.id.studentfragment_recycle);

        mAdapter = new MyClassAdapter(stuList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerview.setLayoutManager(mLayoutManager);
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        recyclerview.setAdapter(mAdapter);
        Analdata();


        return view;
    }

    private void Analdata() {


        MyClassDTO s=new MyClassDTO();
        s.setTitle("Class 5C");
        s.setCounts("20 students");

        s.setSchool("Burnley School");
        stuList.add(s);


        s=new MyClassDTO();
        s.setTitle("Class 5C");
        s.setCounts("20 students");
        s.setSchool("Burnley School");
        stuList.add(s);

        s=new MyClassDTO();
        s.setTitle("Class 5C");
        s.setCounts("20 students");
        s.setSchool("Burnley School");
        stuList.add(s);

        s=new MyClassDTO();
        s.setTitle("Class 5C");
        s.setCounts("20 students");
        s.setSchool("Burnley School");
        stuList.add(s);
    }

}
