package com.school.teacherparent.fragment;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.adapter.AddExamAdapter;
import com.school.teacherparent.models.AddExamDTO;
import com.school.teacherparent.R;

import java.util.ArrayList;
import java.util.List;


public class AddExamFragment extends Fragment {

    private RecyclerView recyclerView;
    private AddExamAdapter mAdapter;
    private List<AddExamDTO> addexamList = new ArrayList<>();
    private Spinner exam_spinner, sec_spinner;
    private RelativeLayout relative_class_spinner, relative_sub_spinner;
    private Spinner class_spinner, sub_spinner;
    private Button add_exm_btn;

    @RequiresApi(api = Build.VERSION_CODES.O)
    public AddExamFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_exam, container, false);
        SidemenuDetailActivity.title.setText(getResources().getString(R.string.add_exam));

        List<String> classpinner = new ArrayList<String>();
        classpinner.add("Term Exam I");
        classpinner.add("Term Exam II");
        classpinner.add("Term Exam III");
        classpinner.add("Term Exam IV");

        exam_spinner = (Spinner)view. findViewById(R.id.exam_spinner);
        add_exm_btn = (Button)view. findViewById(R.id.add_exm_btn);

        add_exm_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Exam Added Successfully", Toast.LENGTH_SHORT).show();
                getActivity().finish();
              /*  Intent intent = new Intent(getApplicationContext(), CircularActivity.class);
                startActivity(intent);*/
            }
        });

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, classpinner);

        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);

        exam_spinner.setAdapter(spinnerArrayAdapter);

        exam_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });


        List<String> sec = new ArrayList<String>();
        sec.add("5A");
        sec.add("6B");
        sec.add("7A");
        sec.add("8A");
        sec.add("9C");
        sec.add("10A");

        sec_spinner = (Spinner) view.findViewById(R.id.sec_spinner);

        ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, sec);

        spinnerArrayAdapter1.setDropDownViewResource(R.layout.spinner_item);

        sec_spinner.setAdapter(spinnerArrayAdapter1);

        sec_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });


        recyclerView = (RecyclerView) view.findViewById(R.id.add_exam_recycle);


        mAdapter = new AddExamAdapter(addexamList);


        //  recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        ExamData();
        return view;
    }

    private void ExamData() {
        AddExamDTO s = new AddExamDTO();
        s.setDate("10 oct 2018");
        s.setTime("9.30 Am");
        addexamList.add(s);

        s = new AddExamDTO();
        s.setDate("10 nov 2018");
        s.setTime("3.30 Pm");
        addexamList.add(s);


        s = new AddExamDTO();
        s.setDate("10 dec 2018");
        s.setTime("3.30 Pm");
        addexamList.add(s);

        s = new AddExamDTO();
        s.setDate("10 Jan 2019");
        s.setTime("3.30 Pm");
        addexamList.add(s);

        s = new AddExamDTO();
        s.setDate("10 Feb 2019");
        s.setTime("5.30 Pm");
        addexamList.add(s);


        s = new AddExamDTO();
        s.setDate("10 Mar 2019");
        s.setTime("2.30 Pm");
        addexamList.add(s);


        s = new AddExamDTO();
        s.setDate("10 Apr 2019");
        s.setTime("10.30 Am");
        addexamList.add(s);

    }
}
