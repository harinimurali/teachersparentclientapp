package com.school.teacherparent.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.school.teacherparent.models.NotiDTO;
import com.school.teacherparent.R;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by keerthana on 9/18/2018.
 */

public class NotiAdapter extends RecyclerView.Adapter<NotiAdapter.MyViewHolder> {

    private  CardView mCardViewBottom;
    private  LinearLayout top;
    private List<NotiDTO> notiList=new ArrayList<>();
    private Context context;
    private LinearLayout header;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, desc, time;
        ImageView img,notiimg,clientimg,whiteimg,userimg;
        LinearLayout lin;


        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            time = (TextView) view.findViewById(R.id.time);
            img=(ImageView)view.findViewById(R.id.img);
            desc=(TextView)view.findViewById(R.id.desc);
            header=(LinearLayout)view.findViewById(R.id.header);
            notiimg=(ImageView)view.findViewById(R.id.noti);
            mCardViewBottom = (CardView)view.findViewById(R.id.top);
            mCardViewBottom.setCardBackgroundColor(Color.parseColor("#ffffff"));
            whiteimg=(ImageView)view.findViewById(R.id.white);
           // userimg=(ImageView) view.findViewById(R.id.userwhiteimg);

            clientimg=(ImageView)view.findViewById(R.id.clientimg);
        }
    }


    public NotiAdapter(List<NotiDTO> notiList, Context context) {
        this.notiList = notiList;
        this.context =context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notifications_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        NotiDTO movie = notiList.get(position);
        holder.title.setText(movie.getTitle());
        holder.time.setText(movie.getTime());
        holder.desc.setText(movie.getDesc());
        holder.img.setImageResource(Integer.parseInt(movie.getImg()));
        holder.notiimg.setImageResource(Integer.parseInt(movie.getNotiimg()));



        if(position==0)
        {
            holder.img.setVisibility(View.GONE);
            holder.clientimg.setVisibility(View.GONE);
            holder.notiimg.setVisibility(View.VISIBLE);
            holder.desc.setVisibility(View.VISIBLE);
            holder.time.setVisibility(View.VISIBLE);
          header.setBackgroundColor(Color.parseColor("#f2f9ff"));

        }
        if (position==1)
        {
            holder.img.setVisibility(View.VISIBLE);
            holder.notiimg.setVisibility(View.VISIBLE);
            holder.clientimg.setVisibility(View.GONE);
            holder.desc.setVisibility(View.VISIBLE);
            holder.time.setVisibility(View.VISIBLE);
 header.setBackgroundColor(Color.parseColor("#f2f9ff"));


        }
        if (position==2)
        {
            holder.img.setVisibility(View.GONE);
            holder.whiteimg.setVisibility(View.VISIBLE);
            holder.notiimg.setVisibility(View.GONE);
            holder.clientimg.setVisibility(View.GONE);
            holder.desc.setVisibility(View.VISIBLE);
            holder.time.setVisibility(View.VISIBLE);
    //  lin.setBackgroundColor(Color.parseColor("#ffffff"));

        }
        if (position==3)
        {
            holder.img.setVisibility(View.VISIBLE);
            holder.notiimg.setVisibility(View.GONE);
            holder.whiteimg.setVisibility(View.VISIBLE);
            holder.clientimg.setVisibility(View.VISIBLE);
            holder.desc.setVisibility(View.VISIBLE);
            holder.time.setVisibility(View.VISIBLE);
     // lin.setBackgroundColor(Color.parseColor("#ffffff"));


        }
    }

    @Override
    public int getItemCount() {
        return notiList.size();
    }
}