package com.school.teacherparent.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.school.teacherparent.Interface.AddTouchListener;
import com.school.teacherparent.models.CircularDTO;
import com.school.teacherparent.R;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by keerthana on 9/12/2018.
 */

public  class CircularAdapter extends RecyclerView.Adapter<CircularAdapter.MyViewHolder> {

    private  LinearLayout top;
    private  ImageView menuu;
    private List<CircularDTO> circularlist=new ArrayList<>();
    private Context context;
    private int[] android_versionnames;
    private int row_index;
    private int focusedItem = 0;
    private int position;
    private View itemView;
    private LinearLayout lin;
    private MotionEvent event;
    private View v;
    private View view;
    private Context con;
    private PopupWindow pwindo;
    private AddTouchListener addTouchListen;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private  View view_border;
        private  CardView mCardViewBottom;
        public TextView title;
        ImageView img;
        public TextView desc;
        public TextView content;
        public LinearLayout lin1;
        private TextView tv1;
        public ImageView menuu;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            desc = (TextView) view.findViewById(R.id.desc);
            content=(TextView)view.findViewById(R.id.content);
            lin1=(LinearLayout)view.findViewById(R.id.lin1);
            lin=(LinearLayout)view.findViewById(R.id.lin);
            top=(LinearLayout)view.findViewById(R.id.top);
            menuu=(ImageView)view.findViewById(R.id.menu);
            view_border=(View)view.findViewById(R.id.view_border);
            mCardViewBottom = (CardView)view.findViewById(R.id.card_view_bottom);
            mCardViewBottom.setCardBackgroundColor(context.getResources().getColor(R.color.gray_background));
        }
    }

/*
    private void initiatepopupwindow() {
        try {
// We need to get the instance of the LayoutInflater
            LayoutInflater inflater = (LayoutInflater) con
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = LayoutInflater.from(con).inflate(R.layout.custom_layout, null);
            pwindo = new PopupWindow(layout, 700, 500, true);
            pwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);


            final TextView subject = (TextView) layout.findViewById(R.id.edit);
            final TextView content = (TextView) layout.findViewById(R.id.delete);



        }
    catch (Exception e)
    {
    }
    }
*/





    public CircularAdapter(List<CircularDTO> circularlist, Context context) {
        this.circularlist = circularlist;
        this.context =context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.circular_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    public void setOnClickListen(AddTouchListener addTouchListen) {
        this.addTouchListen = addTouchListen;
    }



    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
       // holder.tv1.setText(android_versionnames[position]);
        CircularDTO movie = circularlist.get(position);
        holder.title.setText(movie.getTitle());
        holder.desc.setText(movie.getDesc());
        holder.content.setText(movie.getContent());
     //   menuu=(ImageView)v.findViewById(R.id.menu);
        holder.menuu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addTouchListen.onTouchClick(position);

/*
                try {
// We need to get the instance of the LayoutInflater
                    LayoutInflater inflater = (LayoutInflater) context
                            .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View layout = inflater.inflate(R.layout.custom_layout,
                            (ViewGroup)view.findViewById(R.id.top));
                   // View layout = LayoutInflater.from(con).inflate(R.layout.custom_layout, null);
                    pwindo = new PopupWindow(layout, 900, 250, true);
                    pwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);


                    final TextView subject = (TextView) layout.findViewById(R.id.edit);
                    final TextView content = (TextView) layout.findViewById(R.id.delete);



                }
                catch (Exception e)
                {
                }
*/
               /* AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(view.getRootView().getContext());
                alertDialogBuilder.setMessage("Delete");
                alertDialogBuilder.setTitle("Edit");

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();  */


            }
        });


        if(position==0)
        {
            holder.menuu.setVisibility(View.VISIBLE);

        }
        if (position==1)
        {
            holder.view_border.setVisibility(View.VISIBLE);

            lin.setBackgroundColor(Color.parseColor("#ffffff"));
            /*lin.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if(event.getAction() == MotionEvent.ACTION_DOWN)
                    {
                        v.setBackgroundColor(Color.parseColor("#ffffff"));
                    }
                    if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL)
                    {
                        v.setBackgroundColor(Color.TRANSPARENT);
                    }
                    return false;
                }
            });*/
        }


    }

  /*  private void alertdialogbox() {


        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
// ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alert_label_editor, null);
        dialogBuilder.setView(dialogView);

        TextView editText = (EditText) dialogView.findViewById(R.id.edit);
        editText.setText("test label");
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();

*/





      /*  AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(view.getRootView().getContext());
        alertDialogBuilder.setMessage("Are you sure you want to delete?");


        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();*/


    @Override
    public int getItemCount() {
        return circularlist.size();
    }
}