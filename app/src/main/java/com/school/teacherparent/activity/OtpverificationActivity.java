package com.school.teacherparent.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.os.CountDownTimer;

import com.chaos.view.PinView;
import com.msg91.sendotp.library.SendOtpVerification;
import com.msg91.sendotp.library.Verification;
import com.msg91.sendotp.library.VerificationListener;


import com.school.teacherparent.R;

import com.school.teacherparent.utils.Constants;


/**
 * Created by harini on 8/25/2018.
 */


public class OtpverificationActivity extends AppCompatActivity implements VerificationListener {

    private static final String TAG = Verification.class.getSimpleName();
    private Verification mVerification;
    Button verify;
    ImageView resend;
    PinView otp_pin;
    String mobile_number;
    TextView timer, text_resend_otp;

    SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otp_screen);
        Intent i = getIntent();
        mobile_number = i.getStringExtra("phn_number");
        sharedPreferences = getSharedPreferences(Constants.FCMTOKEN, MODE_PRIVATE);

        verify = findViewById(R.id.button_otp_verify);
        resend = findViewById(R.id.image_resend);
        otp_pin = findViewById(R.id.otp_pinView);
        timer = findViewById(R.id.text_timer);
        text_resend_otp = findViewById(R.id.text_resend_otp);

       // callApi();
        // initiateVerification();
        resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initiateVerification();
                Toast.makeText(OtpverificationActivity.this, "OTP sent to your number", Toast.LENGTH_SHORT).show();
                new CountDownTimer(30000, 1000) {

                    public void onTick(long millisUntilFinished) {
                        timer.setVisibility(View.VISIBLE);
                        timer.setText("Retry in " + millisUntilFinished / 1000 + " seconds");
                        resend.setVisibility(View.GONE);
                        text_resend_otp.setVisibility(View.GONE);
                        //here you can have your logic to set text to edittext
                    }

                    public void onFinish() {
                        timer.setVisibility(View.GONE);
                        resend.setVisibility(View.VISIBLE);
                        text_resend_otp.setVisibility(View.VISIBLE);
                    }

                }.start();

            }
        });
        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (otp_pin.getText().toString().length() == 0) {
                    otp_pin.setError("Please Enter OTP");
                } else {
                    startActivity(new Intent(OtpverificationActivity.this, SecurityPinActivity.class));
                   // onSubmitClicked();
                }
            }
        });


        new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                timer.setVisibility(View.VISIBLE);
                timer.setText("Retry in " + millisUntilFinished / 1000 + " seconds");
                resend.setVisibility(View.GONE);
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                timer.setVisibility(View.GONE);
                resend.setVisibility(View.VISIBLE);
            }

        }.start();
    }

    void initiateVerification() {
        initiateVerification(false);
    }

    public void onSubmitClicked() {
        String code = otp_pin.getText().toString();
        if (!code.isEmpty()) {
            hideKeypad();
            if (mVerification != null) {
                mVerification.verify(code);
               /* TextView messageText = (TextView) findViewById(R.id.textView);
                messageText.setText("Verification in progress");*/
                // enableInputField(false);
            }
        }
    }

    void initiateVerification(boolean skipPermissionCheck) {
        //set user number in textview

        if (mobile_number != null) {
            // otpphonenumber.setText("We have sent the verification code to " + "+91" + numb + ". Please Verify.");
            createVerification(mobile_number, skipPermissionCheck, "+91");
        }

    }

    void createVerification(String phoneNumber, boolean skipPermissionCheck, String countryCode) {
       /* if (!skipPermissionCheck && ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_SMS) ==
                PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_SMS}, 0);
            hideProgressBar();
        } else {*/
        mVerification = SendOtpVerification.createSmsVerification
                (SendOtpVerification
                        .config(countryCode + phoneNumber)
                        .context(OtpverificationActivity.this)
                        .autoVerification(true)
                        .build(), this);
        mVerification.initiate();// finall build will be submitted on 22
        //  }
    }


    @Override
    public void onInitiationFailed(Exception paramException) {
        Log.e(TAG, "Verification initialization failed: " + paramException.getMessage());
        Toast.makeText(OtpverificationActivity.this, "Verification Failed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onVerified(String response) {
        // hideProgressBar();
        hideKeypad();
        // callApi();
        //startActivity(new Intent(OtpverificationActivity.this, SecurityPinActivity.class));
    }

    @Override
    public void onInitiated(String response) {
        Log.d(TAG, "Initialized!" + response);
        //OTP successfully resent/sent.
    }

    @Override
    public void onVerificationFailed(Exception exception) {
        Log.e(TAG, "Verification failed: " + exception.getMessage());
        //OTP  verification failed.
    }

    private void hideKeypad() {
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

/*
    public void callApi() {

        String fcmkey = sharedPreferences.getString(Constants.FCMTOKEN, "");
        teacherParentAPI.getLogin(mobile_number, fcmkey, Build.ID, "android", "245421", Build.VERSION.SDK, "1.0", "1", "").enqueue(new Callback<LoginDTO>() {
            @Override
            public void onResponse(Call<LoginDTO> call, Response<LoginDTO> response) {
                //  hideProgress();
                if (response.body() != null) {
                    startActivity(new Intent(OtpverificationActivity.this, SecurityPinActivity.class));
                } else {

                }
            }

            @Override
            public void onFailure(Call<LoginDTO> call, Throwable t) {
                // hideProgress();
            }
        });


    }
*/
}
