package com.school.teacherparent.models;

/**
 * Created by keerthana on 10/1/2018.
 */

public class ResultListDTO {


    private String class_img, stu_img,stu_name,class_name,percent,counts;



    public String getClass_img() {
        return class_img;
    }

    public void setClass_img(String class_img) {
        this.class_img = class_img;
    }

    public String getStu_img() {
        return stu_img;
    }

    public void setStu_img(String stu_img) {
        this.stu_img = stu_img;
    }


    public String getStu_name() {
        return stu_name;
    }

    public void setStu_name(String stu_name) {
        this.stu_name = stu_name;
    }




    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }


    public String getPercent() {
        return percent;
    }

    public void setPercent(String percent) {
        this.percent = percent;


    }






    public String getCounts() {
        return counts;
    }

    public void setCounts(String counts) {
        this.counts = counts;
    }






















}
