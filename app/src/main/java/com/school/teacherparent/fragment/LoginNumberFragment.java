package com.school.teacherparent.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.school.teacherparent.activity.OtpverificationActivity;
import com.school.teacherparent.R;

/**
 * Created by harini on 10/23/2018.
 */

public class LoginNumberFragment extends Fragment {
    TextView skip;
    EditText mPhone_number;
    Button submit;

    public LoginNumberFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_login, container, false);


        submit = view.findViewById(R.id.submit);
        skip = view.findViewById(R.id.skip);
        mPhone_number = view.findViewById(R.id.mobile_number);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPhone_number.getText().toString().isEmpty()) {
                    mPhone_number.setError("Please Enter Phone Number");
                } else if (mPhone_number.getText().toString().length() < 10) {
                    mPhone_number.setError("Please valid Phone Number");
                } else {
                    mPhone_number.setError(null);
                    startActivity(new Intent(getActivity(), OtpverificationActivity.class).putExtra("phn_number", mPhone_number.getText().toString()));
                }
            }
        });
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new LoginUserFragment();
                replaceFragment(fragment);
            }
        });
        return view;
    }

    public void replaceFragment(Fragment fragment) {
        if (fragment != null) {
            android.support.v4.app.FragmentManager fragmentManager = getFragmentManager();
            android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.login_frame, fragment, "");
            fragmentTransaction.commit();
        }
    }
}

