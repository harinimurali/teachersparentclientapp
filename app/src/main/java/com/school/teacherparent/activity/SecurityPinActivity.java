package com.school.teacherparent.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


import com.school.teacherparent.R;

/**
 * Created by harini on 9/24/2018.
 */

public class SecurityPinActivity extends AppCompatActivity {
    TextView exisiting_pin;
    EditText newpin, cnfrmpin;
    Button update;
    boolean valid = false;

    SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_changepin);
        exisiting_pin = findViewById(R.id.pin);
        newpin = findViewById(R.id.new_pin);
        cnfrmpin = findViewById(R.id.confrm_pin);
        update = findViewById(R.id.update);



        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {
                    startActivity(new Intent(SecurityPinActivity.this, TestDrawerActivity.class));
                  //  callApi();
                }
            }
        });

    }

    public boolean validate() {
        String newpn = newpin.getText().toString();
        String cnfrmpn = cnfrmpin.getText().toString();

        if (newpn.length() == 0 || newpn.length() < 4) {
            newpin.setError("Please Enter Valid Pin");
            valid = false;
        } else if (cnfrmpn.length() == 0 || cnfrmpn.length() < 4) {
            cnfrmpin.setError("Please Enter Valid Pin");
            valid = false;
        } else if (!newpn.equals(cnfrmpn)) {
            cnfrmpin.setError("Pin doesn't match");
            valid = false;
        } else {
            newpin.setError(null);
            cnfrmpin.setError(null);
            valid = true;
        }
        return valid;
    }

/*
    public void callApi() {
        teacherParentAPI.getChangePin("1", "11111", newpin.getText().toString()).enqueue(new Callback<LoginDTO>() {
            @Override
            public void onResponse(Call<LoginDTO> call, Response<LoginDTO> response) {
                //  hideProgress();
                if (response.body() != null) {
                    startActivity(new Intent(SecurityPinActivity.this, TestDrawerActivity.class));
                } else {

                }
            }

            @Override
            public void onFailure(Call<LoginDTO> call, Throwable t) {
                // hideProgress();
            }
        });

    }
*/
}
