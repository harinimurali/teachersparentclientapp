package com.school.teacherparent.utils;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by harini on 8/23/2018.
 */

public class FontTextViewBold extends android.support.v7.widget.AppCompatTextView {
    public FontTextViewBold(Context context) {
        super(context);
        setIncludeFontPadding(false);
        setTypeface(ProximaNovaFont.getInstance(context).getBoldTypeFace());
    }

    public FontTextViewBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        setIncludeFontPadding(false);
        setTypeface(ProximaNovaFont.getInstance(context).getBoldTypeFace());
    }

    public FontTextViewBold(Context context, AttributeSet attrs,
                             int defStyle) {
        super(context, attrs, defStyle);
        setIncludeFontPadding(false);
        setTypeface(ProximaNovaFont.getInstance(context).getBoldTypeFace());
    }
}
