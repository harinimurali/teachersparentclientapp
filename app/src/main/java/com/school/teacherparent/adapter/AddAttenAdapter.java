package com.school.teacherparent.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.school.teacherparent.models.AddAttenDTO;
import com.school.teacherparent.R;

import java.util.ArrayList;
import java.util.List;



/**
 * Created by keerthana on 10/10/2018.
 */


public class AddAttenAdapter extends RecyclerView.Adapter<AddAttenAdapter.MyViewHolder> {

    private List<AddAttenDTO> addattenList=new ArrayList<>();
    private Context context;
    public AddTouchListen addTouchListen;
    private Spinner class_spinner;
    private Spinner sub_spinner;




    public AddAttenAdapter(List<AddAttenDTO> addattenList) {
        this.addattenList=addattenList;
        this.context=context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private  SwitchCompat switch_compat;
        private  TextView name;
        private RelativeLayout relative_class_spinner,relative_sub_spinner;
        public TextView title, subtitle, time;
        ImageView img;
        LinearLayout lin;
        public TextView desc,subdesc,date;
        public TextView content;
        private Spinner sub_spinner,class_spinner;

        public MyViewHolder(View view) {
            super(view);
            name=(TextView)view.findViewById(R.id.name);
            img=(ImageView) view.findViewById(R.id.img);
            switch_compat=(SwitchCompat)view.findViewById(R.id.switch_compat);

        }
    }




    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.add_attendance_view, parent, false);



        return new MyViewHolder(itemView);
    }

    public void OnsetClickListen(AddTouchListen addTouchListen)
    {
        this.addTouchListen=addTouchListen;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        AddAttenDTO movie = addattenList.get(position);
      holder.name.setText(movie.getName());
      holder.img.setImageResource(Integer.parseInt(movie.getImg()));

    }

    @Override
    public int getItemCount() {
        return addattenList.size();
    }

    public interface AddTouchListen{
        public void OnTouchClick(int position);

    }
}
