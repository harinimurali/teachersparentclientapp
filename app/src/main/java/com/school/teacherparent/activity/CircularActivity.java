package com.school.teacherparent.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.school.teacherparent.adapter.CircularAdapter;
import com.school.teacherparent.Interface.AddTouchListener;
import com.school.teacherparent.models.CircularDTO;
import com.school.teacherparent.R;

import java.util.ArrayList;


public class CircularActivity extends Activity {

    private RecyclerView recycle;
    private ArrayList<CircularDTO> circularlist = new ArrayList<>();
    private CircularAdapter mAdapter;
    private ImageView back;
    private ImageView menuu;
    private View v;
    private Context context;
    private PopupWindow pwindo;
    private int mViewSpacingLeft;
    private int mViewSpacingTop;
    private int mViewSpacingRight;
    private int mViewSpacingBottom;
    private boolean mViewSpacingSpecified = false;
    FloatingActionButton addcircular;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_circular);


        recycle = (RecyclerView) findViewById(R.id.recycle_circular);
        circularlist = new ArrayList<>();
        AnalData();

        back = (ImageView) findViewById(R.id.back);
        addcircular = findViewById(R.id.fab);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        //  recyclerview.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        mAdapter = new CircularAdapter(circularlist, this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);

        recycle.setLayoutManager(mLayoutManager);

        recycle.setItemAnimator(new DefaultItemAnimator());

        recycle.setAdapter(mAdapter);

        mAdapter.setOnClickListen(new AddTouchListener() {
            @Override
            public void onTouchClick(int position) {

                LayoutInflater inflater = getLayoutInflater();
                View alertLayout = inflater.inflate(R.layout.dialog_custom_edit_delete, null);
                final TextView edit = alertLayout.findViewById(R.id.edit);
                final TextView delete = alertLayout.findViewById(R.id.delete);
                AlertDialog.Builder alert = new AlertDialog.Builder(new ContextThemeWrapper(CircularActivity.this, R.style.actionSheetTheme));
                alert.setView(alertLayout);
                AlertDialog dialog = alert.create();
                dialog.show();
                dialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);
            }
        });
        addcircular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CircularActivity.this, SidemenuDetailActivity.class).putExtra("type","circular"));
            }
        });
    }


    private void AnalData() {

        CircularDTO s = new CircularDTO();
        s.setTitle("Field Trip for Class II");
        s.setContent("14 Sep 2018 at 10Am");
        s.setDesc("The school is organising an educational field trip to Traffic Park.Goshamahal for Students of class");
        circularlist.add(s);

        s = new CircularDTO();
        s.setTitle("Working day on 2nd Saturaday");
        s.setContent("14 Sep 2018 at 5Pm");
        s.setDesc("The school is organising an educational field trip to Traffic Park.Goshamahal for Students of class");
        circularlist.add(s);


        s = new CircularDTO();
        s.setTitle("Tennis coaching in Before and After School hours for Classes X to XII");
        s.setContent("11 Sep at 5Am");
        s.setDesc("The school is organising an educational field trip to Traffic Park.Goshamahal for Students of class");
        circularlist.add(s);


        s = new CircularDTO();
        s.setTitle("Talentia Competition for VI to IX");
        s.setContent("21 Sep 2018 at Afternoon");
        s.setDesc("The school is organising an educational field trip to Traffic Park.Goshamahal for Students of class");
        circularlist.add(s);


        s = new CircularDTO();
        s.setTitle("Quiz Competion for XII");
        s.setContent("10 Sep at 2018");
        s.setDesc("The school is organising an educational field trip to Traffic Park.Goshamahal for Students of class");
        circularlist.add(s);
    }
}
