package com.school.teacherparent.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.adapter.TimeTableAdapter;
import com.school.teacherparent.models.TimeTableDTO;
import com.school.teacherparent.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by harini on 9/26/2018.
 */

public class TimetableFragment extends Fragment {

    RecyclerView recyclerView;
    private TimeTableAdapter mAdapter;
    private List<TimeTableDTO> timeList = new ArrayList<>();
    MaterialCalendarView materialCalendarView;


    public TimetableFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View view = inflater.inflate(R.layout.fragment_timetable, container, false);
        SidemenuDetailActivity.title.setText(getResources().getString(R.string.timetabel));
        recyclerView = view.findViewById(R.id.timetable_recycle);

       /* materialCalendarView = view.findViewById(R.id.material_calender);
        materialCalendarView.state().edit().setFirstDayOfWeek(1).setMinimumDate(CalendarDay.from(2018, 4, 1)).setMaximumDate(CalendarDay.from(2050, 12, 12)).setCalendarDisplayMode(CalendarMode.MONTHS).commit();
        materialCalendarView.setCurrentDate(Calendar.getInstance());
        materialCalendarView.getCurrentDate();*/

        mAdapter = new TimeTableAdapter(timeList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        preData();
        return view;
    }

    private void preData() {

        TimeTableDTO s = new TimeTableDTO();
        s.setClasstext("Class 5C");
        s.setSubtext("English");
        s.setImg(String.valueOf(R.mipmap.english_icon));
        s.setTime("9.00-9.30");
        s.setDuration("1st period");
        timeList.add(s);


        s = new TimeTableDTO();
        s.setClasstext("Class 7C");
        s.setSubtext("Maths");
        s.setImg(String.valueOf(R.mipmap.maths_icon));
        s.setTime("1.00-2.00");
        s.setDuration("3rd period");
        timeList.add(s);


        s = new TimeTableDTO();
        s.setClasstext("Class 8C");
        s.setSubtext("Science");
        s.setImg(String.valueOf(R.mipmap.english_icon));
        s.setTime("2.00-3.00");
        s.setDuration("4th period");
        timeList.add(s);


        s = new TimeTableDTO();
        s.setClasstext("Class 9C");
        s.setSubtext("Social");
        s.setImg(String.valueOf(R.mipmap.maths_icon));
        s.setTime("2.00-5.00");
        s.setDuration("5th period");
        timeList.add(s);


        s = new TimeTableDTO();
        s.setClasstext("Class 10C");
        s.setSubtext("social");
        s.setImg(String.valueOf(R.mipmap.maths_icon));
        s.setTime("5.00-6.00");
        s.setDuration("6th period");
        timeList.add(s);

    }


}
