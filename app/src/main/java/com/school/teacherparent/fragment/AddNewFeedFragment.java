package com.school.teacherparent.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.school.teacherparent.activity.TestDrawerActivity;
import com.school.teacherparent.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by harini on 9/10/2018.
 */

public class AddNewFeedFragment extends Fragment {

    public CheckBox addToGallery;
    TextView add_feed;
    ImageView back;

    @RequiresApi(api = Build.VERSION_CODES.O)
    public AddNewFeedFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_addnewfeed, container, false);

        init(view);

        ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("Name", "Public");
        map.put("Icon", R.mipmap.visiblity_icon);
        list.add(map);

        map = new HashMap<String, Object>();
        map.put("Name", "Private");
        map.put("Icon", R.mipmap.privacy_icon);
        list.add(map);

        Spinner spin = (Spinner) view.findViewById(R.id.spinner);
        myAdapter adapter = new myAdapter(getActivity(), list,
                R.layout.spinner_dropdown_item_image, new String[]{"Name", "Icon"},
                new int[]{R.id.name, R.id.icon});

        spin.setAdapter(adapter);
        return view;

    }

    private void init(View view) {
        add_feed = view.findViewById(R.id.add_feed);

        addToGallery = view.findViewById(R.id.addToGallery);

        Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "proximanova_semibold.otf");
        addToGallery.setTypeface(font);

        add_feed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), TestDrawerActivity.class));
            }
        });


    }

    private class myAdapter extends SimpleAdapter {

        public myAdapter(Context context, List<? extends Map<String, ?>> data,
                         int resource, String[] from, int[] to) {
            super(context, data, resource, from, to);

        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = getLayoutInflater().inflate(R.layout.spinner_dropdown_item_image,
                        null);
            }

            HashMap<String, Object> data = (HashMap<String, Object>) getItem(position);

            ((TextView) convertView.findViewById(R.id.name))
                    .setText((String) data.get("Name"));
            ((ImageView) convertView.findViewById(R.id.icon))
                    .setImageResource((Integer) data.get("Icon"));

            return convertView;
        }

    }
}
