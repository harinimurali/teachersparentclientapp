package com.school.teacherparent.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.school.teacherparent.models.LeaveDTO;
import com.school.teacherparent.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by keerthana on 9/11/2018.
 */
public class LeaveAdapter extends RecyclerView.Adapter<LeaveAdapter.MyViewHolder> {

    private List<LeaveDTO> leaveList=new ArrayList<>();
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, subtitle, time;
        ImageView img;
        LinearLayout lin;
        public TextView desc,subdesc,date;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            subtitle = (TextView) view.findViewById(R.id.subtitle);
            time = (TextView) view.findViewById(R.id.time);
            img=(ImageView)view.findViewById(R.id.imgim);
           desc=(TextView)view.findViewById(R.id.desc);
           subdesc=(TextView)view.findViewById(R.id.subdesc);
           date=(TextView)view.findViewById(R.id.date);
           time=(TextView)view.findViewById(R.id.time);
        }
    }


    public LeaveAdapter(List<LeaveDTO> leaveList, Context context) {
        this.leaveList = leaveList;
        this.context =context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.leave_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        LeaveDTO movie = leaveList.get(position);
        holder.title.setText(movie.getTitle());
        holder.subtitle.setText(movie.getSubtitle());
        holder.desc.setText(movie.getDesc());
        holder.subdesc.setText(movie.getSubdesc());
        holder.date.setText(movie.getDate());
        holder.time.setText(movie.getTime());
        holder.img.setImageResource(Integer.parseInt(movie.getImg()));
    }

    @Override
    public int getItemCount() {
        return leaveList.size();
    }
}