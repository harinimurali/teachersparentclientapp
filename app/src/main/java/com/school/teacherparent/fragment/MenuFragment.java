package com.school.teacherparent.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.school.teacherparent.activity.AttendanceActivity;
import com.school.teacherparent.activity.CircularActivity;
import com.school.teacherparent.activity.ClassworkActivity;
import com.school.teacherparent.activity.EventListActivity;
import com.school.teacherparent.activity.ExamsActivity;
import com.school.teacherparent.activity.HomeWorkActivity;
import com.school.teacherparent.activity.LeaveListActivity;
import com.school.teacherparent.activity.ResultActivity;
import com.school.teacherparent.activity.TestDrawerActivity;
import com.school.teacherparent.adapter.MenuAdapter;
import com.school.teacherparent.R;

/**
 * Created by harini on 8/29/2018.
 */

public class MenuFragment extends Fragment {


    public Integer[] mThumbIds = {
            R.mipmap.menu_attendance, R.mipmap.menu_leave,
            R.mipmap.menu_event, R.mipmap.menu_circular,
            R.mipmap.menu_exam, R.mipmap.menu_result,
            R.mipmap.menu_classwork, R.mipmap.menu_homework
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_menu, container, false);
        TestDrawerActivity.toolbar.setTitle("Menu");


        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        GridView gridView = (GridView) view.findViewById(R.id.grid_view);
        final String[] web = getActivity().getResources().getStringArray(R.array.menu_name);
        gridView.setAdapter(new MenuAdapter(getActivity(), web, mThumbIds));
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                switch (position) {
                    case 0:
                        startActivity(new Intent(getActivity(), AttendanceActivity.class));
                        break;
                    case 1:
                        startActivity(new Intent(getActivity(), LeaveListActivity.class));
                        break;
                    case 2:
                        startActivity(new Intent(getActivity(), EventListActivity.class));
                        break;
                    case 3:
                        startActivity(new Intent(getActivity(), CircularActivity.class));
                        break;
                    case 4:
                          startActivity(new Intent(getActivity(), ExamsActivity.class));
                        break;
                    case 5:
                         startActivity(new Intent(getActivity(), ResultActivity.class));
                        break;
                    case 6:
                        startActivity(new Intent(getActivity(), ClassworkActivity.class));
                        break;
                    case 7:
                         startActivity(new Intent(getActivity(), HomeWorkActivity.class));
                        break;
                }


            }
        });

        // Instance of ImageAdapter Class
    }
}
