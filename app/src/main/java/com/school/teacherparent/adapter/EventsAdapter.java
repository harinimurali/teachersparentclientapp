package com.school.teacherparent.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.school.teacherparent.Interface.AddTouchListener;
import com.school.teacherparent.models.EventsDTO;
import com.school.teacherparent.R;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by keerthana on 9/12/2018.
 */

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.MyViewHolder> {

    private List<EventsDTO> eventList=new ArrayList<>();
    private Context context;
    AddTouchListener addTouchListen;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        ImageView img,menu;
        public TextView desc,date,time;
        public TextView content;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            desc = (TextView) view.findViewById(R.id.desc);
            content=(TextView)view.findViewById(R.id.content);
            img=(ImageView) view.findViewById(R.id.img);
            menu=(ImageView) view.findViewById(R.id.menu);
            date=(TextView)view.findViewById(R.id.date);
            time=(TextView)view.findViewById(R.id.time);

        }
    }

    public void setOnClickListen(AddTouchListener addTouchListen) {
        this.addTouchListen = addTouchListen;
    }
    public EventsAdapter(List<EventsDTO> eventList, Context context) {
        this.eventList = eventList;
        this.context =context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.event_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        EventsDTO movie = eventList.get(position);
        holder.title.setText(movie.getTitle());
        holder.desc.setText(movie.getDesc());
        holder.content.setText(movie.getContent());
        holder.img.setImageResource(Integer.parseInt(movie.getImg()));
        holder.date.setText(movie.getDate());
        holder.time.setText(movie.getTime());

        holder.menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addTouchListen.onTouchClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return eventList.size();
    }
}