package com.school.teacherparent.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;

import com.github.florent37.expansionpanel.ExpansionLayout;
import com.github.florent37.expansionpanel.viewgroup.ExpansionLayoutCollection;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.school.teacherparent.activity.TestDrawerActivity;
import com.school.teacherparent.adapter.DiaryAdapter;
import com.school.teacherparent.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by harini on 8/29/2018.
 */

public class DiaryFragment extends Fragment {
    RecyclerView diary_recycle;
    DiaryAdapter diaryAdapter;
    MaterialCalendarView materialCalendarView;
    List<String> list;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_diary, container, false);
        TestDrawerActivity.toolbar.setTitle("Diary");
        /*FlexibleCalendar viewCalendar = (FlexibleCalendar) view.findViewById(R.id.calendar);
        Calendar cal = Calendar.getInstance();
        CalendarAdapter adapter = new CalendarAdapter(getActivity(), cal);
        viewCalendar.setAdapter(adapter);*/
        diary_recycle = view.findViewById(R.id.diary_recycle);
       /* materialCalendarView = view.findViewById(R.id.material_calender);
        materialCalendarView.state().edit().setFirstDayOfWeek(1).setMinimumDate(CalendarDay.from(2018, 4, 1)).setMaximumDate(CalendarDay.from(2050, 12, 12)).setCalendarDisplayMode(CalendarMode.MONTHS).commit();
        materialCalendarView.setCurrentDate(Calendar.getInstance());
        materialCalendarView.getCalendarMode();


        materialCalendarView.setSelectionMode(SELECTION_MODE_RANGE);*/


       // materialCalendarView.getCurrentDate();

        diaryAdapter = new DiaryAdapter(list, getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        diary_recycle.setLayoutManager(mLayoutManager);
        diary_recycle.setItemAnimator(new DefaultItemAnimator());
        diary_recycle.setAdapter(diaryAdapter);
        diary_recycle.onScrollStateChanged(0);
        setHasOptionsMenu(true);
        final RecyclerAdapter adapter = new RecyclerAdapter();
        //  message_recycle.setAdapter(adapter);
        return view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        getActivity().getMenuInflater().inflate(R.menu.main, menu);
        menu.getItem(0).setIcon(ContextCompat.getDrawable(getActivity(), R.mipmap.search_icon));
        super.onPrepareOptionsMenu(menu);
    }

    public final static class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.RecyclerHolder> {

        private final List<Object> list = new ArrayList<>();

        private final ExpansionLayoutCollection expansionsCollection = new ExpansionLayoutCollection();

        public RecyclerAdapter() {
            expansionsCollection.openOnlyOne(true);
        }

        @Override
        public RecyclerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return RecyclerHolder.buildFor(parent);
        }

        @Override
        public void onBindViewHolder(RecyclerHolder holder, int position) {
            holder.bind(list.get(position));

            expansionsCollection.add(holder.getExpansionLayout());
        }

        @Override
        public int getItemCount() {
            //return list.size();
            return 5;
        }

        public void setItems(List<Object> items) {
            this.list.addAll(items);
            notifyDataSetChanged();
        }

        public final static class RecyclerHolder extends RecyclerView.ViewHolder {

            private static final int LAYOUT = R.layout.diary_list_item;

            //  @BindView(R.id.expansionLayout)
            ExpansionLayout expansionLayout;

            public static RecyclerHolder buildFor(ViewGroup viewGroup) {
                return new RecyclerHolder(LayoutInflater.from(viewGroup.getContext()).inflate(LAYOUT, viewGroup, false));
            }

            public RecyclerHolder(View itemView) {
                super(itemView);
                expansionLayout = itemView.findViewById(R.id.expansionLayout);
                //  ButterKnife.bind(this, itemView);
            }

            public void bind(Object object) {
                expansionLayout.collapse(false);
            }

            public ExpansionLayout getExpansionLayout() {
                return expansionLayout;
            }
        }
    }

}
