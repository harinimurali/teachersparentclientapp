package com.school.teacherparent.repository;

import android.content.Context;
import android.content.SharedPreferences;

import com.school.teacherparent.app.TeachersParentApp;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by harini on 9/24/2018.
 */
//provides instance or objects for a class
@Module
public class AppModule {

    public static final String SCHOOL_PREFS = "school";

    private final TeachersParentApp teachersParentApp;

    public AppModule(TeachersParentApp app) {
        this.teachersParentApp = app;
    }

    //provides dependencies (return application class objects)
    @Provides
    @Singleton
    Context provideApplicationContext() {
        return teachersParentApp;
    }

    //provides dependencies (return sharedprference objects)
    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences() {
        return teachersParentApp.getSharedPreferences(SCHOOL_PREFS, MODE_PRIVATE);
    }

    //provides dependencies (return API class objects)
    @Provides
    public TeacherParentAPI provideSchoolApiInterface(Retrofit retrofit) {
        return retrofit.create(TeacherParentAPI.class);
    }
}
