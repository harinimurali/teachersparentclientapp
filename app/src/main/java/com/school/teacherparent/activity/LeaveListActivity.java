package com.school.teacherparent.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.school.teacherparent.adapter.LeaveAdapter;
import com.school.teacherparent.models.LeaveDTO;
import com.school.teacherparent.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by harini on 9/12/2018.
 */

public class LeaveListActivity extends AppCompatActivity {
    private RecyclerView recyclerview;
    private LeaveAdapter mAdapter;
    private List<LeaveDTO> leaveList = new ArrayList<>();
    ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leave_list);
        recyclerview = (RecyclerView) findViewById(R.id.recycle);
        leaveList = new ArrayList<>();
        AnalData();
        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //  recyclerview.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        mAdapter = new LeaveAdapter(leaveList, LeaveListActivity.this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);

        recyclerview.setLayoutManager(mLayoutManager);

        recyclerview.setItemAnimator(new DefaultItemAnimator());

        recyclerview.setAdapter(mAdapter);

    }

    private void AnalData() {


        LeaveDTO s = new LeaveDTO();
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setTitle("Alexander");
        s.setSubtitle("VII");
        s.setDate("21/08/2018");
        s.setDesc("Absent");
        s.setTime("12 minutes ago");
        s.setSubdesc("Suffering from fever");
        leaveList.add(s);


        s = new LeaveDTO();
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setTitle("Rahul");
        s.setSubtitle("VII");
        s.setDate("21/08/2018");
        s.setDesc("Absent");
        s.setTime("2 minutes ago");
        s.setSubdesc("My son is Suffering from fever. Therefore i request you to kindly excuse him.");
        leaveList.add(s);


        s = new LeaveDTO();
        s.setTitle("Pradeep");
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setSubtitle("VII");
        s.setTime("2 minutes ago");
        s.setDate("21/08/2018");
        s.setDesc("Absent");
        s.setSubdesc("Suffering from fever");
        leaveList.add(s);


        s = new LeaveDTO();
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setTitle("Sandeep");
        s.setSubtitle("VII");
        s.setDate("21/08/2018");
        s.setDesc("Absent");
        s.setTime("10 minutes ago");
        s.setSubdesc("Suffering from fever");
        leaveList.add(s);

    }
}
