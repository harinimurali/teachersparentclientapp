package com.school.teacherparent.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.school.teacherparent.models.SyllabusListDTO;
import com.school.teacherparent.R;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by keerthana on 10/19/2018.
 */

public class SyllabusListAdapter extends RecyclerView.Adapter<SyllabusListAdapter.MyViewHolder> {

    private List<SyllabusListDTO> syllabusList=new ArrayList<>();
    private Context context;
    private AddTouchListen addTouchListen;
    private ProgressBar prg1;
    private ProgressBar Prg1;
    private ProgressBar prg2;

    public SyllabusListAdapter(List<SyllabusListDTO> syllabusList) {

        this.syllabusList=syllabusList;
    }

    /*public ClassAdapter(List<ClassDTO> classList) {
        this.classList = classList;

    }*/

    public class MyViewHolder extends RecyclerView.ViewHolder {

        // private ProgressBar prg1,prg2;
        private TextView counts;
        private ImageView img,img2;
        private  TextView title,content,status1,status2;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title1);
            content = (TextView) view.findViewById(R.id.content1);
            img=(ImageView)view.findViewById(R.id.img);
            counts=(TextView)view.findViewById(R.id.counts);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.syllabus_list_item, parent, false);

        return new MyViewHolder(itemView);
    }
    public void setOnClickListen(AddTouchListen addTouchListen) {
        this.addTouchListen = addTouchListen;
    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        SyllabusListDTO movie = syllabusList.get(position);
        holder.title.setText(movie.getTitle());
        holder.content.setText(movie.getContent());
        holder.img.setImageResource(Integer.parseInt(movie.getImg()));
        holder.counts.setText(movie.getCounts());


        holder.content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (addTouchListen != null) {
                    addTouchListen.onTouchClick(position);
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return syllabusList.size();
    }

    public interface AddTouchListen {
        public void onTouchClick(int position);
    }
}