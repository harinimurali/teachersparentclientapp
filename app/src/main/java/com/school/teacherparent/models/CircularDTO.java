package com.school.teacherparent.models;

/**
 * Created by keerthana on 9/12/2018.
 */

public class CircularDTO {
    private String title,content,desc;



    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }



    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }




    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }







}