package com.school.teacherparent.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.R;

/**
 * Created by harini on 9/25/2018.
 */

public class HelpFragment extends Fragment {

    public HelpFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View view = inflater.inflate(R.layout.fragment_help, container, false);
        SidemenuDetailActivity.title.setText(getResources().getString(R.string.help));
        return view;
    }
}
