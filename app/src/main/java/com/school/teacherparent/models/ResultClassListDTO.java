package com.school.teacherparent.models;

/**
 * Created by keerthana on 10/1/2018.
 */

public class ResultClassListDTO {

    private String img, name,counts;



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }


    public String getCounts() {
        return counts;
    }

    public void setCounts(String counts) {
        this.counts = counts;
    }



}
