package com.school.teacherparent.models;

/**
 * Created by keerthana on 10/10/2018.
 */

public  class AddAttenDTO {
    private String name, img;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

}