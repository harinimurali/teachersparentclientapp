package com.school.teacherparent.models;

/**
 * Created by keerthana on 9/26/2018.
 */


public class TimeTableDTO {
    private String img, classtext,subtext,time,duration;



    public String getClasstext() {
        return classtext;
    }

    public void setClasstext(String classtext) {
        this.classtext = classtext;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }


    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }




    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }



    public String getSubtext() {
        return subtext;
    }

    public void setSubtext(String subtext) {
        this.subtext = subtext;
    }





}