package com.school.teacherparent.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.school.teacherparent.Interface.AddTouchListener;
import com.school.teacherparent.R;

import java.util.List;

/**
 * Created by harini on 9/11/2018.
 */

public class DiaryAdapter extends RecyclerView.Adapter<DiaryAdapter.ViewHolder> {

    private List<String> showing;
    private Context context;
    AddTouchListener addTouchListen;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView filmname, category, status, percentage, views;
        public ImageView image_now_showing;


        public ViewHolder(View view) {
            super(view);
           /* filmname = (TextView) view.findViewById(R.id.text_label_now_showing_film_name);
            category = (TextView) view.findViewById(R.id.text_label_film_category);
            status = (TextView) view.findViewById(R.id.text_label_status);
            percentage = (TextView) view.findViewById(R.id.text_label_now_showing_percentage);
            views = (TextView) view.findViewById(R.id.text_label_now_showing_views);
            image_now_showing = view.findViewById(R.id.image_now_showing);
*/
        }
    }

    public void setOnClickListen(AddTouchListener addTouchListen) {
        this.addTouchListen = addTouchListen;
    }

    public DiaryAdapter(List<String> showing, Context context) {
        this.showing = showing;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.diary_list_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
       /* NowShowing_cl.Result now = showing.get(position);
        holder.filmname.setText(now.getMovie_original_title());
        holder.category.setText(now.getMovie_language());
        holder.status.setText(now.getMovie_certificate().getMovie_certificate());
        holder.percentage.setText(now.getMovie_average() + " %");
        holder.views.setText(now.getMovie_votes() + " views");
        Picasso.with(context).
                load(context.getString(R.string.coming_soon_image_base_url)
                        + now.getMovie_backgroundimg()).placeholder(R.drawable.default_movie).
                into(holder.image_now_showing);
        holder.image_now_showing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addTouchListen.onTouchClick(position);
            }
        });*/
    }

    @Override
    public int getItemCount() {
        //return showing.size();
        return 10;
    }


}

