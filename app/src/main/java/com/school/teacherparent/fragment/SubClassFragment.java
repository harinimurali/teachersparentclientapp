package com.school.teacherparent.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.adapter.SubClassAdapter;
import com.school.teacherparent.models.SubClassDTO;
import com.school.teacherparent.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SubClassFragment extends Fragment {


    private RecyclerView recyclerView;
    private SubClassAdapter mAdapter;
    private List<SubClassDTO> subclassList = new ArrayList<>();
    FloatingActionButton addClasswork;
    ImageView noti, back;
    Fragment fragment;

    public SubClassFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sub_class, container, false);
        noti = view.findViewById(R.id.noti);
        back = view.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragment = new ClassWorkTab();
                replaceFragment(fragment);
            }
        });
        noti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), NotificationActivity.class);
                startActivity(intent);
            }
        });
        recyclerView = (RecyclerView) view.findViewById(R.id.recycle_sub_class);
        addClasswork = view.findViewById(R.id.add_classwork);

        addClasswork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), SidemenuDetailActivity.class).putExtra("type", "classwork"));
            }
        });

        mAdapter = new SubClassAdapter(subclassList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        Analdata();

        return view;
    }

    private void Analdata() {

        SubClassDTO s = new SubClassDTO();
        s.setTitle("English");
        s.setSubtitle("My mother at sixty two");
        s.setContent("Topic:2.4");
        s.setStatus1("Lorium ipsum dolar sit amet,constructor eclipsing elid, sed do elusmud");
        s.setImg(String.valueOf(R.mipmap.english_icon));
        s.setSubimg1(String.valueOf(R.drawable.bg_overlay));
        s.setSubimg2(String.valueOf(R.drawable.bg_overlay));
        subclassList.add(s);


        s = new SubClassDTO();
        s.setTitle("Science");
        s.setSubtitle("Anatomy of Human Beings");
        s.setContent("Topic:6.4");
        s.setStatus1("Human beings skeletal system and collections of Red Blood cells and White Blood cells");
        s.setImg(String.valueOf(R.mipmap.maths_icon));
        s.setSubimg1(String.valueOf(R.drawable.bg_overlay));
        s.setSubimg2(String.valueOf(R.drawable.bg_overlay));
        subclassList.add(s);

        s = new SubClassDTO();
        s.setTitle("Maths");
        s.setSubtitle("Trignomentry Formulae");
        s.setStatus1("Trignometry Formulae of sin,cos,tan theta and describe and solve pythagoras theorem");
        s.setContent("Topic:4");
        s.setImg(String.valueOf(R.mipmap.english_icon));
        s.setSubimg1(String.valueOf(R.drawable.bg_overlay));
        s.setSubimg2(String.valueOf(R.drawable.bg_overlay));
        subclassList.add(s);


        s = new SubClassDTO();
        s.setTitle("Social");
        s.setSubtitle("Indian Constitutional");
        s.setStatus1("Indian constitution of economy and welfare of Indian");
        s.setContent("Topic:8");
        s.setImg(String.valueOf(R.mipmap.maths_icon));
        s.setSubimg1(String.valueOf(R.drawable.bg_overlay));
        s.setSubimg2(String.valueOf(R.drawable.bg_overlay));
        subclassList.add(s);


    }
    private void replaceFragment(Fragment fragment) {
        String backStateName;
        backStateName = ((Object) fragment).getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.frame_classwork, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);//
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }

}
