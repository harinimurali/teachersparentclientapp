package com.school.teacherparent.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.school.teacherparent.models.TimeTableDTO;
import com.school.teacherparent.R;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by keerthana on 9/26/2018.
 */

public class TimeTableAdapter extends RecyclerView.Adapter<TimeTableAdapter.MyViewHolder> {

    private List<TimeTableDTO> timeList=new ArrayList<>();
    private Context context;

    public TimeTableAdapter(List<TimeTableDTO> timeList) {
        this.timeList=timeList;
        this.context=context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView classtext, subtext, time,period;
        ImageView img;


        public MyViewHolder(View view) {
            super(view);
            classtext = (TextView) view.findViewById(R.id.class_text);
            time = (TextView) view.findViewById(R.id.time);
            img=(ImageView)view.findViewById(R.id.dp);
            subtext=(TextView)view.findViewById(R.id.sub_text);
            period=(TextView)view.findViewById(R.id.period);
        }
    }


    public TimeTableAdapter(List<TimeTableDTO> timeList, Context context) {
        this.timeList = timeList;
        this.context =context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.time_table_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        TimeTableDTO movie = timeList.get(position);
        holder.subtext.setText(movie.getSubtext());
        holder.time.setText(movie.getTime());
        holder.classtext.setText(movie.getClasstext());
        holder.period.setText(movie.getDuration());
        holder.img.setImageResource(Integer.parseInt(movie.getImg()));
    }

    @Override
    public int getItemCount() {
        return timeList.size();
    }
}