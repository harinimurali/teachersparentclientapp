package com.school.teacherparent.models;

/**
 * Created by keerthana on 10/5/2018.
 */

public class EventSpinnerDTO {
    String text;
    Integer imageId;
    public EventSpinnerDTO(String text, Integer imageId){
        this.text=text;
        this.imageId=imageId;
    }

    public String getText(){
        return text;
    }

    public Integer getImageId(){
        return imageId;
    }
}
