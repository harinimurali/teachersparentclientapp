package com.school.teacherparent.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.Toast;

import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class SettingFragment extends Fragment implements CompoundButton.OnCheckedChangeListener {


    private Switch chat_switch;
    private Button update;
    RelativeLayout security_pin_layout;

    public SettingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View view = inflater.inflate(R.layout.fragment_setting, container, false);
        chat_switch = (Switch) view.findViewById(R.id.chat_switch);
        update = (Button) view.findViewById(R.id.update);
        security_pin_layout = view.findViewById(R.id.security_pin_layout);

        SidemenuDetailActivity.title.setText(getResources().getString(R.string.settings));


        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "updated successfully", Toast.LENGTH_SHORT).show();
            }
        });

        security_pin_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), SecurityPinActivity.class));
            }
        });
        return view;

    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

    }
}
