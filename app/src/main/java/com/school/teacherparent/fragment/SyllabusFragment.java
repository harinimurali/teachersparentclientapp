package com.school.teacherparent.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.adapter.SyllabusListAdapter;
import com.school.teacherparent.models.SyllabusListDTO;
import com.school.teacherparent.R;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class SyllabusFragment extends Fragment {
    private RecyclerView recyclerView;
    private SyllabusListAdapter mAdapter;
    private List<SyllabusListDTO> syllabusList=new ArrayList<>();

FloatingActionButton addClasswork;
    public SyllabusFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view =  inflater.inflate(R.layout.fragment_syllabus, container, false);
        addClasswork = view.findViewById(R.id.add_classwork);
        addClasswork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), SidemenuDetailActivity.class).putExtra("type","classwork"));
            }
        });

        recyclerView=(RecyclerView)view.findViewById(R.id.recycle_fragment_syllabus);

        mAdapter = new SyllabusListAdapter(syllabusList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        Analdata();
        return view;

    }
    private void Analdata() {
        SyllabusListDTO s=new SyllabusListDTO();
        s.setTitle("Class 5A");
        s.setContent("English");
        s.setImg(String.valueOf(R.mipmap.english_icon));
        s.setCounts("62%");
        syllabusList.add(s);


        s=new SyllabusListDTO();
        s.setTitle("Class 6C");
        s.setContent("English");
        s.setImg(String.valueOf(R.mipmap.english_icon));
        s.setCounts("72%");
        syllabusList.add(s);

        s=new SyllabusListDTO();
        s.setTitle("Class 7C");
        s.setContent("English");
        s.setImg(String.valueOf(R.mipmap.english_icon));
        s.setCounts("50%");
        syllabusList.add(s);

        s=new SyllabusListDTO();
        s.setTitle("Class 8C");
        s.setContent("English");
        s.setImg(String.valueOf(R.mipmap.english_icon));
        s.setCounts("52%");
        syllabusList.add(s);

        s=new SyllabusListDTO();
        s.setTitle("Class 9C");
        s.setContent("English");
        s.setImg(String.valueOf(R.mipmap.english_icon));
        s.setCounts("62%");
        syllabusList.add(s);

        s=new SyllabusListDTO();
        s.setTitle("Class 10C");
        s.setContent("English");
        s.setImg(String.valueOf(R.mipmap.english_icon));
        s.setCounts("65%");
        syllabusList.add(s);
    }


}
