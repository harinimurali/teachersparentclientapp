package com.school.teacherparent.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.school.teacherparent.models.MyClassDTO;
import com.school.teacherparent.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by keerthana on 9/25/2018.
 */

public class MyClassAdapter extends RecyclerView.Adapter<MyClassAdapter.MyViewHolder> {

    private ImageView img;
    private TextView name,std,school;
    private List<MyClassDTO> myclassList=new ArrayList<>();
    private Context context;

    public MyClassAdapter(List<MyClassDTO> myclassList) {
        this.myclassList = myclassList;
        this.context =context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        public TextView classes,counts,school;


        public MyViewHolder(View view) {
            super(view);


            classes = (TextView) view.findViewById(R.id.classes);
            counts = (TextView) view.findViewById(R.id.counts);
            school = (TextView) view.findViewById(R.id.school);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.myclass_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        MyClassDTO movie = myclassList.get(position);
        holder.classes.setText(movie.getTitle());
        holder.counts.setText(movie.getCounts());
        holder.school.setText(movie.getSchool());


    }

    @Override
    public int getItemCount() {
        return myclassList .size();
    }
}