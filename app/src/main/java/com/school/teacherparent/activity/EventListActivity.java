package com.school.teacherparent.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.school.teacherparent.adapter.EventsAdapter;
import com.school.teacherparent.Interface.AddTouchListener;
import com.school.teacherparent.models.EventsDTO;
import com.school.teacherparent.R;

import java.util.ArrayList;

/**
 * Created by harini on 9/12/2018.
 */

public class EventListActivity extends AppCompatActivity {
    private RecyclerView recycle;
    private EventsAdapter mAdapter;
    ImageView back;
    FloatingActionButton addcircular;
    private ArrayList<EventsDTO> eventList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_list);

        recycle = (RecyclerView) findViewById(R.id.recycle_events);
        back = findViewById(R.id.back);
        eventList = new ArrayList<>();
        addcircular = findViewById(R.id.fab);
        AnalData();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //  recyclerview.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        mAdapter = new EventsAdapter(eventList, this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);

        recycle.setLayoutManager(mLayoutManager);

        recycle.setItemAnimator(new DefaultItemAnimator());

        recycle.setAdapter(mAdapter);

        mAdapter.setOnClickListen(new AddTouchListener() {
            @Override
            public void onTouchClick(int position) {

                LayoutInflater inflater = getLayoutInflater();
                View alertLayout = inflater.inflate(R.layout.dialog_custom_edit_delete, null);
                final TextView edit = alertLayout.findViewById(R.id.edit);
                final TextView delete = alertLayout.findViewById(R.id.delete);
                AlertDialog.Builder alert = new AlertDialog.Builder(new ContextThemeWrapper(EventListActivity.this, R.style.actionSheetTheme));
                alert.setView(alertLayout);
                AlertDialog dialog = alert.create();
                dialog.show();
                dialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);
            }
        });
        addcircular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(EventListActivity.this, SidemenuDetailActivity.class).putExtra("type","events"));
            }
        });

    }

    private void AnalData() {

        EventsDTO s = new EventsDTO();
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setTitle("Harvest Fest ");
        s.setContent("added 12 min ago");
        s.setDesc("Principal Burnley High School is fortunate to have a great PTO at her school.The PTO parents");
        s.setDate("15 Sep 2018");
        s.setTime("2.30 Pm");


        eventList.add(s);


        s = new EventsDTO();
        s.setTitle("Arts Celebration");
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setContent("added 10 min ago");
        s.setDesc("March is Youth Art Month,the deal time to turn your's schools spotlight on arts.They do that");
        s.setDate("20 Sep 2018");
        s.setTime("5.30Pm");

        eventList.add(s);


        s = new EventsDTO();
        s.setTitle("Sports Celebration");
        s.setContent("added 3 min ago");
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setDesc("The school is organising an educational field trip to Traffic Park.Goshamahal for Students of class");
        s.setTime("3.30 pm");
        s.setDate("15 Aug 2018");
        eventList.add(s);
//As i having lisence test on 22nd Oct 2018, I need a half day permission. Kindly approve my permission

        s = new EventsDTO();
        s.setTitle("Talentia Competition");
        s.setContent("added 5 min ago");
        s.setDesc("The Speech competition is organising by school for all students");
        s.setTime("10.30 am");
        s.setDate("13 Sep 2018");
        s.setImg(String.valueOf(R.mipmap.student_image));
        eventList.add(s);


    }

}
