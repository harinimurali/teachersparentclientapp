package com.school.teacherparent.models;

/**
 * Created by keerthana on 9/12/2018.
 */

public class EventsDTO {
    private String img,title,content,desc,date,time;



    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }



    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }




    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }



    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }


}