package com.school.teacherparent.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.school.teacherparent.activity.OtpverificationActivity;
import com.school.teacherparent.R;

/**
 * Created by harini on 9/11/2018.
 */

public class LoginUserFragment extends Fragment {
    Button submit;

    public LoginUserFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.login_screen, container, false);


        submit = view.findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), OtpverificationActivity.class));
            }
        });
        return view;
    }
}
