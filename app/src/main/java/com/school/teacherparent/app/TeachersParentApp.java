package com.school.teacherparent.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.multidex.MultiDexApplication;
import android.support.v7.app.AppCompatDelegate;

import com.school.teacherparent.repository.AppComponent;
import com.school.teacherparent.repository.AppModule;
import com.school.teacherparent.repository.DaggerAppComponent;
import com.school.teacherparent.repository.RetrofitModule;


import javax.inject.Inject;

/**
 * Created by harini on 9/24/2018.
 */

public class TeachersParentApp extends MultiDexApplication {
    @Inject
    public SharedPreferences mPrefs;
    private static TeachersParentApp mInstance;

    public static TeachersParentApp getContext() {
        return mInstance;
    }

    private AppComponent mComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        mComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .retrofitModule(new RetrofitModule("http://schoolerp.dci.in/"))
                .build();
        mComponent.inject(this);
    }

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }
    public AppComponent getComponent() {
        return mComponent;
    }

    public static TeachersParentApp from(@NonNull Context context) {
        return (TeachersParentApp) context.getApplicationContext();
    }

}
