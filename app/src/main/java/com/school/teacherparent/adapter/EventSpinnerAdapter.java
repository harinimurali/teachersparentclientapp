package com.school.teacherparent.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.school.teacherparent.models.EventSpinnerDTO;
import com.school.teacherparent.R;

import java.util.ArrayList;


/**
 * Created by keerthana on 10/5/2018.
 */


public class EventSpinnerAdapter extends ArrayAdapter<EventSpinnerDTO> {
    int groupid;
    Activity context;
    ArrayList<EventSpinnerDTO> list;
    LayoutInflater inflater;

    public EventSpinnerAdapter(Activity context, int groupid, int id, ArrayList<EventSpinnerDTO>
            list) {
        super(context, id, list);
        this.list = list;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.groupid = groupid;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = inflater.inflate(groupid, parent, false);
        ImageView imageView = (ImageView) itemView.findViewById(R.id.icon);
        imageView.setImageResource(list.get(position).getImageId());
        TextView textView = (TextView) itemView.findViewById(R.id.name);
        textView.setText(list.get(position).getText());
        return itemView;
    }

    public View getDropDownView(int position, View convertView, ViewGroup
            parent) {
        return getView(position, convertView, parent);

    }
}
