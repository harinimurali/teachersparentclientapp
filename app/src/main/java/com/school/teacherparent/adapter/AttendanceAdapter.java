package com.school.teacherparent.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.school.teacherparent.models.AttDTO;
import com.school.teacherparent.R;

import java.util.ArrayList;
import java.util.List;
/**
 * Created by keerthana on 10/10/2018.
 */


public class AttendanceAdapter extends RecyclerView.Adapter<AttendanceAdapter.MyViewHolder> {

    private List<AttDTO> attList=new ArrayList<>();
    private Context context;
    public AddTouchListen addTouchListen;
    private Spinner class_spinner;
    private Spinner sub_spinner;




    public AttendanceAdapter(List<AttDTO> attList) {
        this.attList=attList;
        this.context=context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final ImageView img1,img2,img3;
        private SwitchCompat switch_compat;
        private TextView name,s1,s2,text1,text2,present_count,absent_count;
        private RelativeLayout relative_class_spinner,relative_sub_spinner;
        public TextView title, subtitle, time,stu_counts,class_text;
        ImageView img,present_img,absent_img;
        LinearLayout lin;
        public TextView desc,subdesc,date;
        public TextView content;
        private Spinner sub_spinner,class_spinner;

        public MyViewHolder(View view) {
            super(view);
            img=(ImageView) view.findViewById(R.id.img);
            img1=(ImageView) view.findViewById(R.id.img1);
            img2=(ImageView) view.findViewById(R.id.img2);
            img3=(ImageView) view.findViewById(R.id.img3);
            s1=(TextView)view.findViewById(R.id.s1);
            s2=(TextView)view.findViewById(R.id.s2);
            text1=(TextView)view.findViewById(R.id.text1);
            text2=(TextView)view.findViewById(R.id.text2);
            present_count=(TextView)view.findViewById(R.id.present_count);
            absent_count=(TextView)view.findViewById(R.id.absent_count);
            present_img=(ImageView) view.findViewById(R.id.present_img);
            absent_img=(ImageView) view.findViewById(R.id.absent_img);
            stu_counts=(TextView)view.findViewById(R.id.stu_counts);
            class_text=(TextView)view.findViewById(R.id.class_text);
            lin=(LinearLayout)view.findViewById(R.id.lin);

        }
    }




    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.attendance_fragment_item, parent, false);



        return new MyViewHolder(itemView);
    }

    public void OnsetClickListen(AddTouchListen addTouchListen)
    {
        this.addTouchListen=addTouchListen;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        AttDTO movie = attList.get(position);
        holder.class_text.setText(movie.getClass_text());
        holder.stu_counts.setText(movie.getStu_counts());
        holder.s1.setText(movie.getS1());
        holder.s2.setText(movie.getS2());
        holder.text1.setText(movie.getText1());
        holder.text2.setText(movie.getText2());
        holder.present_count.setText(movie.getPresent_count());
        holder.absent_count.setText(movie.getAbsent_count());
      /*  holder.img.setImageResource(Integer.parseInt(movie.getImg()));
        holder.img1.setImageResource(Integer.parseInt(movie.getImg1()));
        holder.img2.setImageResource(Integer.parseInt(movie.getImg2()));
       holder.img3.setImageResource(Integer.parseInt(movie.getImg3()));*/

       holder.lin.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               if(addTouchListen!=null)
               {
                   addTouchListen.OnTouchClick(position);
               }
           }
       });

    }

    @Override
    public int getItemCount() {
        return attList.size();
    }

    public interface AddTouchListen{
        public void OnTouchClick(int position);

    }
}
