package com.school.teacherparent.adapter;

import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.school.teacherparent.R;


/**
 * Created by keerthana on 10/15/2018.
 */

public class ChatAppMsgViewHolder extends RecyclerView.ViewHolder {

    private  CardView card_left,card_right;
    LinearLayout leftMsgLayout;

    LinearLayout rightMsgLayout;

    TextView leftMsgTextView;

    TextView rightMsgTextView;

    public ChatAppMsgViewHolder(View itemView) {
        super(itemView);

        if(itemView!=null) {

            card_left=(CardView)itemView.findViewById(R.id.card_left);
            card_right=(CardView)itemView.findViewById(R.id.card_right);
            leftMsgLayout = (LinearLayout) itemView.findViewById(R.id.chat_left_msg_layout);
            rightMsgLayout = (LinearLayout) itemView.findViewById(R.id.chat_right_msg_layout);
            leftMsgTextView = (TextView) itemView.findViewById(R.id.chat_left_msg_text_view);
            rightMsgTextView = (TextView) itemView.findViewById(R.id.chat_right_msg_text_view);

            card_left.setCardBackgroundColor(Color.parseColor("#ffffff"));
            card_right.setCardBackgroundColor(Color.parseColor("#F3ECE7"));
        }
    }
}