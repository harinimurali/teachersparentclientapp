package com.school.teacherparent.models;

/**
 * Created by keerthana on 9/11/2018.
 */
public class MessageDTO {
    private String img, title,content,time;



    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }


    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }




    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }







}