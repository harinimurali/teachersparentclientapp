package com.school.teacherparent.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.school.teacherparent.adapter.NotiAdapter;
import com.school.teacherparent.models.NotiDTO;
import com.school.teacherparent.R;

import java.util.ArrayList;


public class NotificationActivity extends Activity {

    private RecyclerView recycle;
    private ArrayList<NotiDTO> notiList = new ArrayList<>();
    private ImageView back;
    private NotiAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        recycle = (RecyclerView) findViewById(R.id.recycle_notifications);
        back = (ImageView) findViewById(R.id.back);
        notiList = new ArrayList<>();
        PreData();

        back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        // recycle.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        mAdapter = new NotiAdapter(notiList, this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);

        recycle.setLayoutManager(mLayoutManager);

        recycle.setItemAnimator(new DefaultItemAnimator());
        // recycle.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.VERTICAL, 5));
        recycle.setAdapter(mAdapter);
    }

    private void PreData() {

        NotiDTO s = new NotiDTO();
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setNotiimg(String.valueOf(R.drawable.orange_circle));
        s.setTitle("Circular sent by admin");
        s.setDesc("The School is organizing an educational field trip to Traffic Park-Goshamal for student of class II");
        s.setTime("10 min ago");


        notiList.add(s);


        s = new NotiDTO();
        s.setTitle("Juan sent you a message");
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setNotiimg(String.valueOf(R.drawable.orange_circle));
        s.setDesc("Hello teacher I would know about my son in classroom and his performance in his studies");
        s.setTime("5 min ago");
        notiList.add(s);


        s = new NotiDTO();
        s.setTitle("Circular sent by Emy werly");
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setNotiimg(String.valueOf(R.drawable.orange_circle));
        s.setDesc("The School is organizing an educational field trip to Traffic Park-Goshamal for student of class II");
        s.setTime("15 min ago");
        notiList.add(s);


        s = new NotiDTO();
        s.setTitle("Michael posted a feed");
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setNotiimg(String.valueOf(R.drawable.orange_circle));
        s.setDesc("The School is organizing an educational field trip to Traffic Park-Goshamal for student of class II");
        s.setTime("2 min ago");
        notiList.add(s);

    }
}
