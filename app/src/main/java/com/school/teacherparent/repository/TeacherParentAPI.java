package com.school.teacherparent.repository;

import com.school.teacherparent.models.LoginDTO;

import dagger.Module;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by harini on 9/24/2018.
 */
@Module
public interface TeacherParentAPI {
    @FormUrlEncoded
    @POST("api/login")
    Call<LoginDTO> getLogin(@Field("mobile_no") String email,
                            @Field("fcm_key") String password,
                            @Field("device_id") String device_id,
                            @Field("device_name") String device_name,
                            @Field("device_imei") String device_imei,
                            @Field("device_os") String device_os,
                            @Field("app_version") String app_version,
                            @Field("login_type") String logintype,
                            @Field("security_pin") String securitypin);

    @FormUrlEncoded
    @POST("api/change_pin")
    Call<LoginDTO> getChangePin(@Field("parent_id") String id,
                                @Field("security_pin") String securitypin,
                                @Field("new_security_pin") String newpin);
}
