package com.school.teacherparent.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.school.teacherparent.Interface.AddTouchListener;
import com.school.teacherparent.models.MessageDTO;
import com.school.teacherparent.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by harini on 9/10/2018.
 */

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.ViewHolder> {

    private List<MessageDTO> messagelist = new ArrayList<>();
    private Context context;
    AddTouchListener addTouchListen;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView title, content, date;
        ImageView img;


        public ViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.name);
            date = (TextView) view.findViewById(R.id.date);
            img=(ImageView)view.findViewById(R.id.img);
            content=(TextView)view.findViewById(R.id.detail_name);

        }
    }

    public void setOnClickListen(AddTouchListener addTouchListen) {
        this.addTouchListen = addTouchListen;
    }

    public MessageAdapter(List<MessageDTO> showing, Context context) {
        this.messagelist = showing;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.message_list_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        MessageDTO movie = messagelist.get(position);
        holder.title.setText(movie.getTitle());
        holder.date.setText(movie.getTime());
        holder.content.setText(movie.getContent());
       // holder.img.setImageResource(Integer.parseInt(movie.getImg()));
    }

    @Override
    public int getItemCount() {
        return messagelist.size();
       // return 7;
    }


}


