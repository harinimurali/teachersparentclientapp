package com.school.teacherparent.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.adapter.ResultClassListAdapter;
import com.school.teacherparent.models.ResultClassListDTO;
import com.school.teacherparent.R;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class ResultClassListFragment extends Fragment {


    private Spinner exam_spinner, sub_spinner;
    private RecyclerView recyclerView;
    private ResultClassListAdapter mAdapter;
    private List<ResultClassListDTO> resclassList = new ArrayList<>();
    FloatingActionButton addResult;
    ImageView noti, back;

    public ResultClassListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_result_class, container, false);
        exam_spinner = (Spinner) view.findViewById(R.id.exam_spinner);


        List<String> term = new ArrayList<String>();
        term.add("Ist Term Exam");
        term.add("IInd Term Exam");
        term.add("IIIrd Term Exam");
        term.add("Ist Mid Term Exam");
        term.add("IInd Mid Term Exam");
        term.add("IIIrd Mid Term Exam");

        noti = view.findViewById(R.id.noti);
        back = view.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new ResultFragment();
                replaceFragment(fragment);
            }
        });
        noti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), NotificationActivity.class);
                startActivity(intent);
            }
        });
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, term);

        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);

        exam_spinner.setAdapter(spinnerArrayAdapter);

        exam_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });


        List<String> sub = new ArrayList<String>();
        sub.add("English");
        sub.add("Maths");
        sub.add("Science");
        sub.add("Social");
        sub.add("Economics");
        sub.add("EVS");
        sub.add("Computer Science");


        sub_spinner = (Spinner) view.findViewById(R.id.sub_spinner);

        ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, sub);

        spinnerArrayAdapter1.setDropDownViewResource(R.layout.spinner_item);

        sub_spinner.setAdapter(spinnerArrayAdapter1);

        sub_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });


        recyclerView = (RecyclerView) view.findViewById(R.id.recycle_resultclass_members);

        addResult = view.findViewById(R.id.add_result);

        addResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), SidemenuDetailActivity.class).putExtra("type", "result"));
            }
        });
        Resultdata();
        mAdapter = new ResultClassListAdapter(resclassList, getActivity());
        //   recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(mAdapter);

        mAdapter.setOnclickListen(new ResultClassListAdapter.AddTouchListen() {
            @Override
            public void OnTouchClick(int position) {
                Fragment fragment = new ResultStudentFragment();
                replaceFragment(fragment);
            }
        });


       /* mAdapter.setOnClickListen(new ResultHomeAdapter.AddTouchListen() {
            @Override
            public void onTouchClick(int position) {
                Fragment fragment=new ResultClassFragment();
                replaceFragment(fragment);
            }


        });

*/
        return view;

    }

    private void replaceFragment(Fragment fragment) {
        String backStateName;
        backStateName = ((Object) fragment).getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.result_frame, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);//
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }

    private void Resultdata() {

        ResultClassListDTO s = new ResultClassListDTO();
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setCounts("90%");
        s.setName("Angela chu");
        resclassList.add(s);


        s = new ResultClassListDTO();
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setCounts("76%");
        s.setName("Stephen");
        resclassList.add(s);

        s = new ResultClassListDTO();
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setCounts("60%");
        s.setName("Lord Williams");
        resclassList.add(s);

        s = new ResultClassListDTO();
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setCounts("86%");
        s.setName("John");
        resclassList.add(s);

        s = new ResultClassListDTO();
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setCounts("76%");
        s.setName("Maxine");
        resclassList.add(s);

        s = new ResultClassListDTO();
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setCounts("96%");
        s.setName("James");
        resclassList.add(s);

        s = new ResultClassListDTO();
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setCounts("76%");
        s.setName("Williams");
        resclassList.add(s);

    }

}
