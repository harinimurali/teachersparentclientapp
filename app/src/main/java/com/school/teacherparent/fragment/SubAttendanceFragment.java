package com.school.teacherparent.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.adapter.SubAttendanceAdapter;
import com.school.teacherparent.models.SubAttDTO;
import com.school.teacherparent.R;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class SubAttendanceFragment extends Fragment {


    private RecyclerView recyclerView;
    private SubAttendanceAdapter mAdapter;
    private List<SubAttDTO> subattList=new ArrayList<>();
    FloatingActionButton add_attendance;
    ImageView back;
    public SubAttendanceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_sub_attendance, container, false);

        recyclerView=(RecyclerView)view.findViewById(R.id.sub_attendnce_recycle_fragment);

        mAdapter = new SubAttendanceAdapter(subattList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        add_attendance = view.findViewById(R.id.add_attendance);
        add_attendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), SidemenuDetailActivity.class).putExtra("type", "attendance"));
            }
        });
        AttData();
        back = view.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new AttendanceFragment();
                replaceFragment(fragment);
            }
        });

        return view;
    }

    private void replaceFragment(Fragment fragment) {
        String backStateName;
        backStateName = ((Object) fragment).getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.frame_attendance, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);//
            ft.addToBackStack(backStateName);
            ft.commit();
        }


    }

    private void AttData() {

        SubAttDTO s=new SubAttDTO();
       // s.setStu_img(String.valueOf(R.drawable.user));
        s.setStu_name("Alexander");
        s.setPercentage("80%");
        s.setStatus(String.valueOf(R.drawable.present));
        subattList.add(s);


        s=new SubAttDTO();
       // s.setStu_img(String.valueOf(R.drawable.dp));
        s.setStu_name("Allan");
        s.setPercentage("100%");
        s.setStatus(String.valueOf(R.drawable.present));
        subattList.add(s);



        s=new SubAttDTO();
       // s.setStu_img(String.valueOf(R.drawable.usergirl));
        s.setStu_name("Daniel");
        s.setPercentage("60%");
        s.setStatus(String.valueOf(R.drawable.absent));
        subattList.add(s);



        s=new SubAttDTO();
      //  s.setStu_img(String.valueOf(R.drawable.dp));
        s.setStu_name("Charles Mick");
        s.setPercentage("80%");
        s.setStatus(String.valueOf(R.drawable.absent));
        subattList.add(s);



        s=new SubAttDTO();
      //  s.setStu_img(String.valueOf(R.drawable.usergirl));
        s.setStu_name("Dansi");
        s.setPercentage("90%");
        s.setStatus(String.valueOf(R.drawable.present));
        subattList.add(s);


        s=new SubAttDTO();
     //   s.setStu_img(String.valueOf(R.drawable.user));
        s.setStu_name("Jhansi");
        s.setPercentage("90%");
        s.setStatus(String.valueOf(R.drawable.present));
        subattList.add(s);


        s=new SubAttDTO();
      //  s.setStu_img(String.valueOf(R.drawable.dp));
        s.setStu_name("Klinton");
        s.setPercentage("90%");
        s.setStatus(String.valueOf(R.drawable.present));
        subattList.add(s);

        s=new SubAttDTO();
       // s.setStu_img(String.valueOf(R.drawable.usergirl));
        s.setStu_name("Caroline");
        s.setPercentage("60%");
        s.setStatus(String.valueOf(R.drawable.absent));
        subattList.add(s);


        s=new SubAttDTO();
       // s.setStu_img(String.valueOf(R.drawable.dp));
        s.setStu_name("Michael");
        s.setPercentage("60%");
        s.setStatus(String.valueOf(R.drawable.absent));
        subattList.add(s);

        s=new SubAttDTO();
       // s.setStu_img(String.valueOf(R.drawable.user));
        s.setStu_name("John");
        s.setPercentage("80%");
        s.setStatus(String.valueOf(R.drawable.present));
        subattList.add(s);

        s=new SubAttDTO();
       // s.setStu_img(String.valueOf(R.drawable.dp));
        s.setStu_name("Jaun");
        s.setPercentage("90%");
        s.setStatus(String.valueOf(R.drawable.present));
        subattList.add(s);

        s=new SubAttDTO();
       // s.setStu_img(String.valueOf(R.drawable.usergirl));
        s.setStu_name("James");
        s.setPercentage("90%");
        s.setStatus(String.valueOf(R.drawable.present));
        subattList.add(s);

        s=new SubAttDTO();
      //  s.setStu_img(String.valueOf(R.drawable.user));
        s.setStu_name("Lord Williams");
        s.setPercentage("50%");
        s.setStatus(String.valueOf(R.drawable.absent));
        subattList.add(s);


    }

}
