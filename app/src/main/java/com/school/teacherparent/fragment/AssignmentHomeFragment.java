package com.school.teacherparent.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.school.teacherparent.R;

/**
 * Created by harini on 10/22/2018.
 */

public class AssignmentHomeFragment extends Fragment {

    public AssignmentHomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sub_home, container, false);
        return view;
    }
}
