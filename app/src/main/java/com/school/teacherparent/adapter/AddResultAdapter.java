package com.school.teacherparent.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.school.teacherparent.models.AddResultDTO;
import com.school.teacherparent.R;

import java.util.ArrayList;
import java.util.List;



/**
 * Created by keerthana on 10/5/2018.
 */


public class AddResultAdapter extends RecyclerView.Adapter<AddResultAdapter.MyViewHolder> {

    private List<AddResultDTO> addresList=new ArrayList<>();
    private Context context;
    private int selectedPosition=-1;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private  CheckBox check;
        public TextView  name;
        ImageView img;


        public MyViewHolder(View view) {
            super(view);

            img=(ImageView)view.findViewById(R.id.img);
            name=(TextView)view.findViewById(R.id.name);
            check=(CheckBox)view.findViewById(R.id.check);
        }
    }


    public AddResultAdapter(List<AddResultDTO> addresList, Context context) {
        this.addresList = addresList;
        this.context =context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.result_add_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        AddResultDTO movie = addresList.get(position);
      holder.name.setText(movie.getName());
        holder.img.setImageResource(Integer.parseInt(movie.getImg()));
       /* holder.check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                
            }
        });
        holder.check.setChecked(position == selectedPosition);
        holder.check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (position == selectedPosition) {
                    holder.check.setChecked(false);
                    selectedPosition = -1;
                } else {
                    selectedPosition = position;
                    notifyDataSetChanged();
                }
            }
        });*/

    }

    @Override
    public int getItemCount() {
        return addresList.size();
    }
}