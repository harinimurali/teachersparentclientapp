package com.school.teacherparent.models;

/**
 * Created by keerthana on 10/18/2018.
 */


public class ClapListDTO {
    private String stu_img,stu_name;

    public String getStu_name() {
        return stu_name;
    }

    public void setStu_name(String stu_name) {
        this.stu_name = stu_name;
    }

    public String getStu_img() {
        return stu_img;
    }

    public void setStu_img(String stu_img) {
        this.stu_img = stu_img;
    }
    }

