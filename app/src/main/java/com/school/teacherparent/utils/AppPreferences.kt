package com.school.teacherparent.utils

import android.content.Context
import android.preference.PreferenceManager

/**
 * Created by harini on 8/21/2018.
 */
class AppPreferences{
    fun setFCMToken(context: Context, value: String) {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        val ed = sp.edit()
        ed.putString(Constants.FCMTOKEN, value)
        ed.apply();
    }

    fun getFcmToken(context: Context): String {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        return sp.getString(Constants.FCMTOKEN, "")

    }

}