package com.school.teacherparent.models;

/**
 * Created by keerthana on 9/25/2018.
 */


public class ParentsOfDTO {
    private String img, title,content,school;



    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }


    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }




    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }







}