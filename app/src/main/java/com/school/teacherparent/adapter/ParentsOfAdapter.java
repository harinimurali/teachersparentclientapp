package com.school.teacherparent.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.school.teacherparent.models.ParentsOfDTO;
import com.school.teacherparent.R;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by keerthana on 9/25/2018.
 */


public class ParentsOfAdapter extends RecyclerView.Adapter<ParentsOfAdapter.MyViewHolder> {

    private  ImageView img;
    private TextView name,std,school;
    private List<ParentsOfDTO> parentsList=new ArrayList<>();
    private Context context;

    public ParentsOfAdapter(List<ParentsOfDTO> parentsList) {
        this.parentsList = parentsList;
        this.context =context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        public TextView name,std,school;
        public ImageView img;

        public MyViewHolder(View view) {
            super(view);

            img=(ImageView)view.findViewById(R.id.user);
            name = (TextView) view.findViewById(R.id.name);
            std = (TextView) view.findViewById(R.id.std);
            school = (TextView) view.findViewById(R.id.school);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.parentsof_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ParentsOfDTO movie = parentsList.get(position);
        holder.name.setText(movie.getTitle());
        holder.std.setText(movie.getContent());
        holder.school.setText(movie.getSchool());
        holder.img.setImageResource(Integer.parseInt(movie.getImg()));

    }

    @Override
    public int getItemCount() {
        return parentsList.size();
    }
}