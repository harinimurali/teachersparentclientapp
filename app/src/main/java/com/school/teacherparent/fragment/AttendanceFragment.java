package com.school.teacherparent.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.adapter.AttendanceAdapter;
import com.school.teacherparent.models.AttDTO;
import com.school.teacherparent.R;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class AttendanceFragment extends Fragment {


    private RecyclerView recyclerView;
    private AttendanceAdapter mAdapter;
    private List<AttDTO> attList = new ArrayList<>();
    FloatingActionButton add_attendance;
    ImageView back;

    public AttendanceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_attendance, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.attendance_fragment_recycle);



        add_attendance = view.findViewById(R.id.add_attendance);
        add_attendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), SidemenuDetailActivity.class).putExtra("type", "attendance"));
            }
        });

        back = view.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
        mAdapter = new AttendanceAdapter(attList);
        //  recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        AttenData();


        mAdapter.OnsetClickListen(new AttendanceAdapter.AddTouchListen() {
            @Override
            public void OnTouchClick(int position) {
                Fragment fragment = new SubAttendanceFragment();
                replaceFragment(fragment);
            }

        });
        return view;
    }

    private void replaceFragment(Fragment fragment) {
        String backStateName;
        backStateName = ((Object) fragment).getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.frame_attendance, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);//
            ft.addToBackStack(backStateName);
            ft.commit();
        }


    }


    private void AttenData() {
        AttDTO s = new AttDTO();
        s.setClass_text("Class 5A");
        s.setPresent_count("30");
        s.setAbsent_count("5");
        s.setS1("01");
        s.setText1("Sick Leave");
        s.setText2("Absent");
        s.setS2("01");
        s.setStu_counts("35 students");
        attList.add(s);

        s = new AttDTO();
        s.setClass_text("Class 5B");
        s.setPresent_count("40");
        s.setAbsent_count("4");
        s.setS1("01");
        s.setText1("Leave");
        s.setText2("Absent");
        s.setS2("02");
        s.setStu_counts("44 students");
        attList.add(s);

        s = new AttDTO();
        s.setClass_text("Class 5A");
        s.setPresent_count("30");
        s.setAbsent_count("5");
        s.setS1("01");
        s.setText1("Sick Leave");
        s.setText2("Absent");
        s.setS2("01");
        s.setStu_counts("35 students");
        attList.add(s);

        s = new AttDTO();
        s.setClass_text("Class 7B");
        s.setPresent_count("40");
        s.setAbsent_count("4");
        s.setS1("01");
        s.setText1("Leave");
        s.setText2("Absent");
        s.setS2("02");
        s.setStu_counts("44 students");
        attList.add(s);

        s = new AttDTO();
        s.setClass_text("Class 5A");
        s.setPresent_count("30");
        s.setAbsent_count("5");
        s.setS1("01");
        s.setText1("Sick Leave");
        s.setText2("Absent");
        s.setS2("01");
        s.setStu_counts("35 students");
        attList.add(s);
    }

}
