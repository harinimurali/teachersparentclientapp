package com.school.teacherparent.fragment;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.internal.NavigationMenu;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.activity.TestDrawerActivity;
import com.school.teacherparent.adapter.FeedsAdapter;
import com.school.teacherparent.R;

import java.util.List;

import io.github.yavski.fabspeeddial.FabSpeedDial;
import io.github.yavski.fabspeeddial.SimpleMenuListenerAdapter;

/**
 * Created by harini on 8/27/2018.
 */

public class FeedsFragment extends Fragment {
    RecyclerView feeds_recycle;
    FeedsAdapter feedsAdapter;
    List<String> theatersAdapter;
    Toolbar toolbar;
    FabSpeedDial fabSpeedDial;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_feeds_list, container, false);
        TestDrawerActivity.toolbar.setTitle("Feeds");

        feeds_recycle = view.findViewById(R.id.feeds_recycle);
        feedsAdapter = new FeedsAdapter(theatersAdapter, getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        feeds_recycle.setLayoutManager(mLayoutManager);
        feeds_recycle.setItemAnimator(new DefaultItemAnimator());
        feeds_recycle.setAdapter(feedsAdapter);

        feedsAdapter.setOnClickListen(new FeedsAdapter.AddTouchListen() {
            @Override
            public void onShareClick(int position) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = "Share Feed";
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
            }

            @Override
            public void onTouchClick(int position) {

                LayoutInflater inflater = getLayoutInflater();
                View alertLayout = inflater.inflate(R.layout.dialog_custom_edit_delete, null);
                final TextView edit = alertLayout.findViewById(R.id.edit);
                final TextView delete = alertLayout.findViewById(R.id.delete);
                AlertDialog.Builder alert = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.actionSheetTheme));
                alert.setView(alertLayout);
                AlertDialog dialog = alert.create();
                dialog.show();
                dialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);
            }

            @Override
            public void onCommentClick(int position) {

            }

            @Override
            public void onClapsClick(int position) {

            }

            @Override
            public void onClapsCountClick(int position) {
                startActivity(new Intent(getActivity(), SidemenuDetailActivity.class).putExtra("type", "claps"));
            }
        });
        fabSpeedDial = view.findViewById(R.id.fab_menu);

        fabSpeedDial.setMenuListener(new SimpleMenuListenerAdapter() {
            @Override
            public boolean onPrepareMenu(NavigationMenu navigationMenu) {

                if (fabSpeedDial.isSelected()) {
                    Log.e("open", ">>");
                } else {
                    Log.e("close", ">>" + navigationMenu.toString().trim());
                }
                // TODO: Do something with yout menu items, or return false if you don't want to show them
                return true;
            }

            @Override
            public boolean onMenuItemSelected(MenuItem menuItem) {
                String menuitem = menuItem.toString();
                if (menuitem.equalsIgnoreCase("Add Feed")) {
                    //  getActivity().getResources().getString(R.string.add_feed))) {
                    Log.e("open", ">>" + menuItem);
                    startActivity(new Intent(getActivity(), SidemenuDetailActivity.class).putExtra("type", "feed"));
                }
                return super.onMenuItemSelected(menuItem);
            }

        });

        return view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        getActivity().getMenuInflater().inflate(R.menu.main, menu);
        menu.getItem(0).setIcon(ContextCompat.getDrawable(getActivity(), R.mipmap.search_icon));
        super.onPrepareOptionsMenu(menu);
    }
}

