package com.school.teacherparent.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.school.teacherparent.adapter.ExamDetailAdapter;
import com.school.teacherparent.models.ExamDetailDTO;
import com.school.teacherparent.R;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class ExamDetailListFragment extends Fragment {


    private Spinner spinner;
    private RecyclerView recyclerView;
    private ExamDetailAdapter mAdapter;
    private List<ExamDetailDTO> exam3List=new ArrayList<>();

    public ExamDetailListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_exam_detail, container, false);
        spinner=(Spinner)view.findViewById(R.id.spinner);
        recyclerView=(RecyclerView)view.findViewById(R.id.recycle_exams3fragment);



        mAdapter = new ExamDetailAdapter(exam3List);
        //  recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);



       /* mAdapter.setOnClickListen(new ExamDetailAdapter.AddTouchListen() {
            @Override
            public void OnTouchClick(int position) {
                Intent intent=new Intent(getActivity(), AddExamFragment.class);
                startActivity(intent);
            }
        });*/
        Exam3Data();

        List<String> classpinner = new ArrayList<String>();
        classpinner.add("Class 5C");
        classpinner.add("Class 4C");
        classpinner.add("Class 6C");
        classpinner.add("Class 7A");
        classpinner.add("Class 8B");
        classpinner.add("Class 9C");


        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, classpinner);

        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);

        spinner.setAdapter(spinnerArrayAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });







        return view;
    }

    private void Exam3Data() {
        ExamDetailDTO s=new ExamDetailDTO();
        s.setImg(String.valueOf(R.mipmap.english_icon));
        s.setSubject("English");
        s.setTopic("Topic:4.5");
        s.setDate("10 oct 2018");
        s.setTime("10.30 Am");
        s.setSyllabus("Shakesphere poem");
        s.setDesc("Shakesphere Poem was mind blowing and very enthusiastic in nature");
        exam3List.add(s);

         s=new ExamDetailDTO();
        s.setImg(String.valueOf(R.mipmap.maths_icon));
        s.setSubject("Maths");
        s.setTopic("Topic:4.5");
        s.setDate("11 oct 2018");
        s.setTime("10.30 Am");
        s.setSyllabus("Trignometry Formulae");
        s.setDesc("Trignometry Formulae was constructed by sin,cos,tan theta it defines the Hypotenuse Formulae of Pythagoras theorem");
        exam3List.add(s);


        s=new ExamDetailDTO();
        s.setImg(String.valueOf(R.mipmap.english_icon));
        s.setSubject("Science");
        s.setTopic("Topic:9.5");
        s.setDate("12 oct 2018");
        s.setTime("10.30 Am");
        s.setDesc("Human Beings sense organ, erythrocytes and lymphocides are in Anatomy Units");
        s.setSyllabus("Anatomy of Human parts");
        exam3List.add(s);

        s=new ExamDetailDTO();
        s.setImg(String.valueOf(R.mipmap.maths_icon));
        s.setSubject("Social");
        s.setTopic("Topic:5.5");
        s.setDate("10 oct 2018");
        s.setTime("14.30 Am");
        s.setDesc("Modern Cultural Vs Ancient Cultural");
        s.setSyllabus("Cultural");
        exam3List.add(s);

        s=new ExamDetailDTO();
        s.setImg(String.valueOf(R.mipmap.english_icon));
        s.setSubject("Economics");
        s.setTopic("Topic:5.0");
        s.setDate("19 oct 2018");
        s.setTime("10.30 Am");
        s.setDesc("Calculating a net income by dividing gross domestic product by national income, As a geogrphical boundareies of India The GDP is greater than GNP , GNP is a Gross Net Product");
        s.setSyllabus("Gross Domestic product");
        exam3List.add(s);
    }

}
