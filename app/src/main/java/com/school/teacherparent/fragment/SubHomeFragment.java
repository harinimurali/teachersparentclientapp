package com.school.teacherparent.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.school.teacherparent.adapter.HomeAdapter;
import com.school.teacherparent.models.HomeDTO;
import com.school.teacherparent.R;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class SubHomeFragment extends Fragment {


    private RecyclerView recyclerView;
    private List<HomeDTO> homeList = new ArrayList<>();
    private HomeAdapter mAdapter;

    public SubHomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sub_home, container, false);


        recyclerView = (RecyclerView) view.findViewById(R.id.recycle_sub_home);
        Analdata();
        mAdapter = new HomeAdapter(homeList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        mAdapter.setOnClickListen(new HomeAdapter.AddTouchListen() {
            @Override
            public void onTouchClick(int position) {
                Fragment fragment = new HomeWorkClassWiseTabFragment();
                replaceFragment(fragment);
            }
        });


     /* mAdapter.setOnClickListen(new HomeAdapter.AddTouchListen() {
          @Override
          public void onTouchClick(int position) {
              Fragment fragment=new Sub_category_homeFragment();
              replaceFragment(fragment);
          }
      });
*/
/*

recyclerView.setOnTouchListener(new View.OnTouchListener() {
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {

        Fragment fragment=new Sub_category_homeFragment();
        replaceFragment(fragment);


        return false;
    }
});
*/


        return view;

    }

    private void replaceFragment(Fragment fragment) {

        String backStateName;
        backStateName = ((Object) fragment).getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.frame_homework, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);//
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }

    private void Analdata() {

        HomeDTO s = new HomeDTO();
        s.setImg1(String.valueOf(R.mipmap.profile));
        s.setClassname("Class 5C");
        s.setSubname("English");
        s.setTitle("My mother at sixty two");
        s.setSubtitle("Topic:3.4");
        s.setCounts("87");
        s.setTitle2("Shakesphere Poem");
        s.setSubtitle2("Topic:4.3");
        s.setDate1("Today");
        s.setDate2("26/09/2018");
        homeList.add(s);


        s = new HomeDTO();
        s.setImg1(String.valueOf(R.mipmap.profile));
        s.setClassname("Class 6C");
        s.setSubname("Maths");
        s.setTitle("Trignometry Formulae");
        s.setSubtitle("Topic:4.4");
        s.setCounts("80");
        s.setTitle2("Shakesphere Poem");
        s.setSubtitle2("Topic:4.3");
        s.setDate1("Today");
        s.setDate2("26/09/2018");
        homeList.add(s);


        s = new HomeDTO();
        s.setImg1(String.valueOf(R.mipmap.profile));
        s.setClassname("Class 8C");
        s.setSubname("Science");
        s.setTitle("Anatomy of Human beings");
        s.setSubtitle("Topic:7.4");
        s.setCounts("87");
        s.setTitle2("Lord Williams");
        s.setSubtitle2("Topic:8.3");
        s.setDate1("Today");
        s.setDate2("26/09/2018");
        homeList.add(s);


        s = new HomeDTO();
        s.setImg1(String.valueOf(R.mipmap.profile));
        s.setClassname("Class 5C");
        s.setSubname("English");
        s.setTitle("My mother at sixty two");
        s.setSubtitle("Topic:3.4");
        s.setCounts("87");
        s.setTitle2("Shakesphere Poem");
        s.setSubtitle2("Topic:4.3");
        s.setDate1("Today");
        s.setDate2("26/09/2018");
        homeList.add(s);


        s = new HomeDTO();
        s.setImg1(String.valueOf(R.mipmap.profile));
        s.setClassname("Class 5C");
        s.setSubname("English");
        s.setTitle("My mother at sixty two");
        s.setSubtitle("Topic:3.4");
        s.setCounts("87");
        s.setTitle2("Shakesphere Poem");
        s.setSubtitle2("Topic:4.3");
        s.setDate1("Today");
        s.setDate2("26/09/2018");
        homeList.add(s);


    }

}
