package com.school.teacherparent.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.school.teacherparent.models.SubAttDTO;
import com.school.teacherparent.R;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by keerthana on 10/11/2018.
 */


public class SubAttendanceAdapter extends RecyclerView.Adapter<SubAttendanceAdapter.MyViewHolder> {

    private List<SubAttDTO> subattList = new ArrayList<>();
    private Context context;
    public AddTouchListen addTouchListen;


    public SubAttendanceAdapter(List<SubAttDTO> subattList) {

        this.subattList = subattList;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView counts,stu_name,percentage;;
        private ImageView img, subimg1, subimg2;
        private TextView title, content, subtitle;
        private ImageView stu_img,status;
        public LinearLayout lin;

        public MyViewHolder(View view) {
            super(view);
           stu_img=(ImageView)view.findViewById(R.id.stu_img);
           stu_name=(TextView)view.findViewById(R.id.stu_name);
           status=(ImageView)view.findViewById(R.id.status);
           percentage=(TextView)view.findViewById(R.id.percentage);
           lin=(LinearLayout)view.findViewById(R.id.lin);



        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.sub_attendance_fragment_view_item, parent, false);

        return new MyViewHolder(itemView);
    }

    public void setOnClickListen(AddTouchListen addTouchListen)

    {
        this.addTouchListen = addTouchListen;

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        SubAttDTO movie = subattList.get(position);
       /* holder.stu_img.setImageResource(Integer.parseInt(movie.getStu_img()));
        holder.status.setImageResource(Integer.parseInt(movie.getStatus()));*/
        holder.percentage.setText(movie.getPercentage());
        holder.stu_name.setText(movie.getStu_name());


        holder.lin.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {

                if (addTouchListen != null) {
                    addTouchListen.OnTouchClick(position);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return subattList.size();
    }

    public interface AddTouchListen {
        public void OnTouchClick(int position);

    }
}