package com.school.teacherparent.repository;

import android.content.SharedPreferences;

import com.school.teacherparent.activity.OtpverificationActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.app.TeachersParentApp;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;

/**
 * Created by harini on 9/24/2018.
 */
@Singleton
// modules for perform dependency injection on below classes
@Component(modules = {AppModule.class, RetrofitModule.class})
public interface AppComponent {

    SharedPreferences sharedPreferences();
    Retrofit retrofit();
    void inject(TeachersParentApp teachersParentApp);

    void inject(OtpverificationActivity otpverificationActivity);

    void inject(SecurityPinActivity securityPinActivity);
}
