package com.school.teacherparent.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.adapter.ClapAdapter;
import com.school.teacherparent.models.ClapListDTO;
import com.school.teacherparent.R;

import java.util.ArrayList;
import java.util.List;



/**
 * Created by keerthana on 10/19/2018.
 */

public class FeedClapFragment extends Fragment {

    private RecyclerView recyclerView;
    private EditText comment_edit_text;
    private ImageView post;
    private SwipeRefreshLayout swiper;
    private ClapAdapter mAdapterClapActivity;
    private List<ClapListDTO> claplist=new ArrayList<>();

    public FeedClapFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_feed_clap, container, false);
        recyclerView=view.findViewById(R.id.claps_recycle);
        SidemenuDetailActivity.title.setText(getResources().getString(R.string.clap));
        mAdapterClapActivity = new ClapAdapter(claplist);
        //   recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(mAdapterClapActivity);


        ClapData();


        return view;
    }

    private void ClapData() {
        ClapListDTO s=new ClapListDTO();
        s.setStu_img(String.valueOf(R.mipmap.profile));
        s.setStu_name("Alexander");
        claplist.add(s);

        s=new ClapListDTO();
        s.setStu_img(String.valueOf(R.mipmap.profile));
        s.setStu_name("Allan");
        claplist.add(s);


        s=new ClapListDTO();
        s.setStu_img(String.valueOf(R.mipmap.profile));
        s.setStu_name("Caroline");
        claplist.add(s);


        s=new ClapListDTO();
        s.setStu_img(String.valueOf(R.mipmap.profile));
        s.setStu_name("Daniel");
        claplist.add(s);

        s=new ClapListDTO();
        s.setStu_img(String.valueOf(R.mipmap.profile));
        s.setStu_name("Grahambell");
        claplist.add(s);

        s=new ClapListDTO();
        s.setStu_img(String.valueOf(R.mipmap.profile));
        s.setStu_name("Kingsly");
        claplist.add(s);

        s=new ClapListDTO();
        s.setStu_img(String.valueOf(R.mipmap.profile));
        s.setStu_name("Michael");
        claplist.add(s);

        s=new ClapListDTO();
        s.setStu_img(String.valueOf(R.mipmap.profile));
        s.setStu_name("George");
        claplist.add(s);

    }

}
