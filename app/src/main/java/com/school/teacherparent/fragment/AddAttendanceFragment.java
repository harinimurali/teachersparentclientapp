package com.school.teacherparent.fragment;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.adapter.AddAttenAdapter;
import com.school.teacherparent.models.AddAttenDTO;
import com.school.teacherparent.R;

import java.util.ArrayList;
import java.util.List;


public class AddAttendanceFragment extends Fragment {

    private Spinner class_spinner;
    private RecyclerView recyclerView;
    private TextView present;
    private AddAttenAdapter mAdapter;
    private List<AddAttenDTO> addattenList = new ArrayList<>();
    FloatingActionButton addAttendance;

    public AddAttendanceFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_attendance, container, false);

        class_spinner = (Spinner) view.findViewById(R.id.class_spinner);
        recyclerView = (RecyclerView) view.findViewById(R.id.add_attendance_recycle);
        addAttendance = view.findViewById(R.id.add_attendance);
        SidemenuDetailActivity.title.setText(getResources().getString(R.string.add_attendance));
        mAdapter = new AddAttenAdapter(addattenList);

        addAttendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
        //  recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        AttenData();
        present = (TextView) view.findViewById(R.id.present);

        present.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Attendance Capture Successfully", Toast.LENGTH_SHORT).show();
                /*Intent intent=new Intent(getApplicationContext(),NewMessageActivity.class);
                startActivity(intent);*/
            }
        });

        List<String> categories = new ArrayList<String>();
        categories.add("Class 5A");
        categories.add("Class 5B");
        categories.add("Class 5C");
        categories.add("Class 6A");
        categories.add("Class 6B");
        categories.add("Class 6C");

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, categories);

        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);

        class_spinner.setAdapter(spinnerArrayAdapter);

        class_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        return view;
    }

    private void AttenData() {

        AddAttenDTO s = new AddAttenDTO();
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setName("Alexander");
        addattenList.add(s);

        s = new AddAttenDTO();
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setName("Allan");
        addattenList.add(s);


        s = new AddAttenDTO();
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setName("BristoBrak");
        addattenList.add(s);

        s = new AddAttenDTO();
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setName("CharlesMick");
        addattenList.add(s);

        s = new AddAttenDTO();
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setName("David Cooper");
        addattenList.add(s);

        s = new AddAttenDTO();
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setName("DavCone");
        addattenList.add(s);

        s = new AddAttenDTO();
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setName("Daniel");
        addattenList.add(s);
        s = new AddAttenDTO();
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setName("Dansi");
        addattenList.add(s);

        s = new AddAttenDTO();
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setName("Elessa");
        addattenList.add(s);

    }
}
