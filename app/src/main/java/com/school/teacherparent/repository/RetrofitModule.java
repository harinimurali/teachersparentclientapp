package com.school.teacherparent.repository;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.school.teacherparent.BuildConfig;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by harini on 9/24/2018.
 */

@Module
public class RetrofitModule {
    String mBaseUrl;

    public RetrofitModule(String baseUrl) {
        this.mBaseUrl = baseUrl;
    }

    @Provides
    @Singleton
    Interceptor provideInterceptor() {
        Interceptor interceptorAPI = new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request request = null;
                try {
                    request = chain.request().newBuilder()
                            .addHeader("Content-Type", "application/json")
                            .addHeader("WSH", "U2VjcmV0OnBhbmR0YXV0aGN8UGFzc3dvcmQ6IyRlZnJIeWhhNjQ3")
                            .method(original.method(), original.body())
                            .build();
                } catch (Exception authFailureError) {
                    authFailureError.printStackTrace();
                }
                okhttp3.Response response = chain.proceed(request);

                return response;
            }
        };
        return interceptorAPI;
    }

    @Provides
    @Singleton
    Gson provideGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        return gsonBuilder.create();
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(Interceptor interceptor) {

        OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder();
        okHttpBuilder.interceptors().add(interceptor);
        okHttpBuilder.readTimeout(1, TimeUnit.MINUTES);
        okHttpBuilder.connectTimeout(1, TimeUnit.MINUTES);
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            okHttpBuilder.interceptors().add(logging);
        }
        OkHttpClient client = okHttpBuilder.build();

        return client;
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(Gson gson, OkHttpClient okHttpClient) {
        Retrofit retrofit =
                new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create(gson))
                        .baseUrl(mBaseUrl)
                        .client(okHttpClient)
                        .build();
        return retrofit;
    }
}


