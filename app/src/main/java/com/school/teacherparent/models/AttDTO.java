package com.school.teacherparent.models;

/**
 * Created by keerthana on 10/10/2018.
 */


public class AttDTO {
    private String  img,img1,img2,img3,class_text,stu_counts,present_count,absent_count,s1,s2,text1,text2;



    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getImg1() {
        return img1;
    }

    public void setImg1(String img1) {
        this.img1 = img1;
    }


    public String getImg2() {
        return img2;
    }

    public void setImg2(String img2) {
        this.img2 = img2;
    }


    public String getImg3() {
        return img3;
    }

    public void setImg3(String img3) {
        this.img3 = img3;
    }



    public String getClass_text() {
        return class_text;
    }

    public void setClass_text(String class_text) {
        this.class_text = class_text;
    }



    public String getStu_counts() {
        return stu_counts;
    }

    public void setStu_counts(String stu_counts) {
        this.stu_counts = stu_counts;
    }


    public String getPresent_count() {
        return present_count;
    }

    public void setPresent_count(String present_count) {
        this.present_count = present_count;
    }


    public String getAbsent_count() {
        return absent_count;
    }

    public void setAbsent_count(String absent_count) {
        this.absent_count = absent_count;
    }

       public String getS1() {
        return s1;
    }

    public void setS1(String s1) {
        this.s1 = s1;
    }


    public String getS2() {
        return s2;
    }

    public void setS2(String s2) {
        this.s2 = s2;
    }


    public String getText1() {
        return text1;
    }

    public void setText1(String text1) {
        this.text1 = text1;
    }

    public String getText2() {
        return text2;
    }

    public void setText2(String text2) {
        this.text2 = text2;
    }
}