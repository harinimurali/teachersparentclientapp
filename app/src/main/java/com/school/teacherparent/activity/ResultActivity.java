package com.school.teacherparent.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.school.teacherparent.fragment.ResultFragment;
import com.school.teacherparent.R;


public class ResultActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        Fragment fragment = new ResultFragment();
        replaceFragment(fragment);

        /*result_spinner = (Spinner) findViewById(R.id.result_spinner);
        recyclerView = (RecyclerView) findViewById(R.id.recycle_result_home);

        mAdapter = new ResultHomeAdapter(resList, this);
        //   recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);

        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(mAdapter);

        Resultdata();


        List<String> term = new ArrayList<String>();
        term.add("Ist Term Exam");
        term.add("IInd Term Exam");
        term.add("IIIrd Term Exam");
        term.add("Ist Mid Term Exam");
        term.add("IInd Mid Term Exam");
        term.add("IIIrd Mid Term Exam");


        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, term);

        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);

        result_spinner.setAdapter(spinnerArrayAdapter);

        result_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });
*/

    }

    private void replaceFragment(Fragment fragment) {
        String backStateName;
        backStateName = ((Object) fragment).getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.result_frame, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);//
            ft.addToBackStack(backStateName);
            ft.commit();
        }


    }

    @Override
    public void onBackPressed() {
        Log.e("back_arrow pressed", "back_arrow pressed");
        FragmentManager manager = getSupportFragmentManager();
        Log.e("manager", "entrycount" + manager.getBackStackEntryCount());
        if (manager.getBackStackEntryCount() == 1) {
            finish();
        } else {
            super.onBackPressed();
        }
    }
/*
    private void Resultdata() {
        ResulthomePojo s=new ResulthomePojo();
       s.setClass_img(String.valueOf(R.drawable.classimg));
       s.setStu_img(String.valueOf(R.drawable.dp));
       s.setClass_name("Class 5C");
       s.setStu_name("Mark Zuckerbak");
       s.setPercent("90%");
       s.setCounts("43 students");
        resList.add(s);


        s=new ResulthomePojo();
        s.setClass_img(String.valueOf(R.drawable.classimg));
        s.setStu_img(String.valueOf(R.drawable.dp));
        s.setClass_name("Class 6C");
        s.setStu_name("Robert Luiensten");
        s.setPercent("60%");
        s.setCounts("40 students");
        resList.add(s);


        s=new ResulthomePojo();
        s.setClass_img(String.valueOf(R.drawable.classimg));
        s.setStu_img(String.valueOf(R.drawable.dp));
        s.setClass_name("Class 7C");
        s.setStu_name("Mark Albert Einstein");
        s.setPercent("100%");
        s.setCounts("60 students");
        resList.add(s);


        s=new ResulthomePojo();
        s.setClass_img(String.valueOf(R.drawable.classimg));
        s.setStu_img(String.valueOf(R.drawable.dp));
        s.setClass_name("Class 9C");
        s.setStu_name("Newton");
        s.setPercent("80%");
        s.setCounts("35 students");
        resList.add(s);

        s=new ResulthomePojo();
        s.setClass_img(String.valueOf(R.drawable.classimg));
        s.setStu_img(String.valueOf(R.drawable.dp));
        s.setClass_name("Class 10C");
        s.setStu_name("Robert Williams");
        s.setPercent("98%");
        s.setCounts("13 students");
        resList.add(s);
    }
*/
}
