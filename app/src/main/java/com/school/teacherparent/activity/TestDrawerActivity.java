package com.school.teacherparent.activity;


import android.content.Intent;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.school.teacherparent.adapter.DrawerAdapter;
import com.school.teacherparent.fragment.DiaryFragment;
import com.school.teacherparent.fragment.FeedsFragment;
import com.school.teacherparent.fragment.MenuFragment;
import com.school.teacherparent.fragment.MessageFragment;
import com.school.teacherparent.fragment.ProfileFragment;
import com.school.teacherparent.R;

import java.util.ArrayList;

import io.github.yavski.fabspeeddial.FabSpeedDial;

/**
 * Created by harini on 8/22/2018.
 */

public class TestDrawerActivity extends AppCompatActivity implements AHBottomNavigation.OnTabSelectedListener {
    String sidemenu[];
    private DrawerLayout drawer;
    ListView listView;
    public static Toolbar toolbar;
    private ActionBarDrawerToggle drawerToggle;
    AHBottomNavigation bottomNavigationView;
    FabSpeedDial fabSpeedDial;
    ImageView profile;
    Fragment fragment;
    boolean doubleBackToExitPressedOnce = false;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_drawer);
        sidemenu = getResources().getStringArray(R.array.system);
        listView = (ListView) findViewById(R.id.navdrawer);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawer = findViewById(R.id.drawer_layout);
        profile = findViewById(R.id.contactimage);
        bottomNavigationView = (AHBottomNavigation) findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnTabSelectedListener(this);
        bottomNavigationView.setAccentColor(getResources().getColor(R.color.colorPrimary));

        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setSubtitleTextColor(Color.WHITE);
       /* drawerToggle = new ActionBarDrawerToggle(this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };

        drawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.app_name, R.string.app_name);
        drawer.setDrawerListener(drawerToggle);
*/

        //  mDrawerList = (ListView) findViewById(R.id.navdrawer);
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.closeDrawers();
                Fragment fragment2 = new ProfileFragment();
                replaceFragment(fragment2);
            }
        });

        createitems();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                android.app.FragmentManager fragmentManager = getFragmentManager();
                switch (position) {
                    case 0:
                        drawer.closeDrawers();
                        startActivity(new Intent(TestDrawerActivity.this,SidemenuDetailActivity.class).putExtra("type","school"));
                      /*  Fragment fragment = new HomePageFragment();
                        replaceFragment(fragment);
                        //setTitle("Edukool Parent");
                        title.setText(getResources().getString(R.string.app_name));*/

                        break;

                    case 1:
                        drawer.closeDrawers();
                        startActivity(new Intent(TestDrawerActivity.this,SidemenuDetailActivity.class).putExtra("type","timetable"));
                       /* Fragment fragment1 = new TimetableFragment();
                        replaceFragment(fragment1);*/

                        break;
                    case 2:
                        drawer.closeDrawers();
                        startActivity(new Intent(TestDrawerActivity.this,SidemenuDetailActivity.class).putExtra("type","gallery"));
                       /* Fragment fragmentt = new RecentActivities();
                        replaceFragment(fragmentt);*/
                        break;
                    case 3:
                        drawer.closeDrawers();
                        startActivity(new Intent(TestDrawerActivity.this,SidemenuDetailActivity.class).putExtra("type","settings"));
                       /* Fragment fragment2 = new SettingFragment();
                        replaceFragment(fragment2);*/
                        break;
                    case 4:
                        drawer.closeDrawers();
                        startActivity(new Intent(TestDrawerActivity.this,SidemenuDetailActivity.class).putExtra("type","help"));
                       /* Fragment result = new HelpFragment();
                        replaceFragment(result);*/
                        break;

                }
            }
        });

        DrawerAdapter adapter1 = new DrawerAdapter(TestDrawerActivity.this, sidemenu);
        listView.setAdapter(adapter1);

        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(drawerToggle);
        drawerToggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.white));
        drawerToggle.syncState();

     /*   if (toolbar != null) {
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.mipmap.noti_white_icon);
            toolbar.setTitleTextColor(getResources().getColor(R.color.abc_primary_text_material_dark));
            toolbar.setTitle("Edukool Parent");
        }*/

    }

    private void createitems() {

        AHBottomNavigationItem homeitem = new AHBottomNavigationItem(getString(R.string.feed), R.mipmap.feed_icon);
        AHBottomNavigationItem theatreitem = new AHBottomNavigationItem(getString(R.string.diary), R.mipmap.diary_icon);
        AHBottomNavigationItem searchitem = new AHBottomNavigationItem(getString(R.string.message), R.mipmap.messaage_icon);
        AHBottomNavigationItem settingitem = new AHBottomNavigationItem(getString(R.string.menu), R.mipmap.menu_icon);

        //      AHBottomNavigationItem dashItem=new AHBottomNavigationItem(getString(R.string.dashboard),R.drawable.dashboard_icon);
        //     AHBottomNavigationItem contactItem=new
        bottomNavigationView.addItem(homeitem);
        bottomNavigationView.addItem(theatreitem);
        bottomNavigationView.addItem(searchitem);
        bottomNavigationView.addItem(settingitem);

        bottomNavigationView.setCurrentItem(0);

        bottomNavigationView.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);
        bottomNavigationView.setTranslucentNavigationEnabled(true);

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Feeds");
        }
        //centerToolbarTitle(toolbar);
    }


    @Override
    public boolean onTabSelected(int position, boolean wasSelected) {
        if (position == 0) {
            fragment = new FeedsFragment();
            replaceFragment(fragment);
        } else if (position == 1) {
            fragment = new DiaryFragment();
            replaceFragment(fragment);
        } else if (position == 2) {
            fragment = new MessageFragment();
            replaceFragment(fragment);
        } else if (position == 3) {
            fragment = new MenuFragment();
            replaceFragment(fragment);

        }
        return true;
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

   /* @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }
*/

    public void replaceFragment(Fragment fragment) {
        if (fragment != null) {
            android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
            android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.main, fragment, "");
            fragmentTransaction.commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_notification:
                startActivity(new Intent(TestDrawerActivity.this, NotificationActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public static void centerToolbarTitle(final Toolbar toolbar) {
        final CharSequence title = toolbar.getTitle();
        final ArrayList<View> outViews = new ArrayList<>(1);
        toolbar.findViewsWithText(outViews, title, View.FIND_VIEWS_WITH_TEXT);
        if (!outViews.isEmpty()) {
            final TextView titleView = (TextView) outViews.get(0);
            titleView.setGravity(Gravity.CENTER_HORIZONTAL);
            final Toolbar.LayoutParams layoutParams = (Toolbar.LayoutParams) titleView.getLayoutParams();
            layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
            layoutParams.setMargins(0, 0, 60, 0);
            toolbar.requestLayout();
        }
    }
    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {


            Log.e("back_arrow pressed", "back_arrow pressed");

            FragmentManager manager = getSupportFragmentManager();

            Log.e("manager", "entrycount" + manager.getBackStackEntryCount());

            if (manager.getBackStackEntryCount() == 1) {

           /* // Toast.makeText(MainActivity.this, "skjfghgsdfhjdsf", Toast.LENGTH_SHORT).show();
            if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis()) {
                super.onBackPressed();
                return;
            } else {
                Toast.makeText(getBaseContext(), "Tap back_arrow button in order to exit", Toast.LENGTH_SHORT).show();
            }*/

                //  mBackPressed = System.currentTimeMillis();
                String backStateName;
                backStateName = ((Object) new FeedsFragment()).getClass().getName();
                String fragmentTag = backStateName;

                FragmentTransaction ft = manager.beginTransaction();
                ft.replace(R.id.main, new FeedsFragment(), fragmentTag);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.addToBackStack(backStateName);
                ft.commit();

                if (doubleBackToExitPressedOnce) {
                    finish();
                }

                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);
            }
            super.onBackPressed();
        }

    }

}
