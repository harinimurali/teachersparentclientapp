package com.school.teacherparent.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.school.teacherparent.models.ExamDetailDTO;
import com.school.teacherparent.R;

import java.util.ArrayList;
import java.util.List;



/**
 * Created by keerthana on 10/8/2018.
 */


public class ExamDetailAdapter extends RecyclerView.Adapter<ExamDetailAdapter.MyViewHolder> {

    private List<ExamDetailDTO> exam3List=new ArrayList<>();
    private Context context;
    public AddTouchListen addTouchListen;


    public ExamDetailAdapter(List<ExamDetailDTO> exam3List) {
        this.exam3List = exam3List;
        this.context =context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private  LinearLayout footer;
        public ImageView img;
       public TextView subject,syllabus,topic,date,time,desc;

        public MyViewHolder(View view) {
            super(view);
            subject = (TextView) view.findViewById(R.id.subject);
            time = (TextView) view.findViewById(R.id.time);
            img=(ImageView)view.findViewById(R.id.img);
            date=(TextView)view.findViewById(R.id.date);
            desc=(TextView)view.findViewById(R.id.desc);
            syllabus=(TextView)view.findViewById(R.id.syllabus);
            topic=(TextView)view.findViewById(R.id.topic);
            footer=(LinearLayout)view.findViewById(R.id.footer);
        }
    }




    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.exam_detail_item, parent, false);

        return new MyViewHolder(itemView);
    }
    public void setOnClickListen(AddTouchListen addTouchListen)

    {
        this.addTouchListen=addTouchListen;

    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        ExamDetailDTO movie = exam3List.get(position);

        holder.time.setText(movie.getTime());
        holder.img.setImageResource(Integer.parseInt(movie.getImg()));
        holder.subject.setText(movie.getSubject());
        holder.syllabus.setText(movie.getSyllabus());
        holder.date.setText(movie.getDate());
        holder.desc.setText(movie.getDesc());
        holder.topic.setText(movie.getTopic());


        holder.footer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(addTouchListen!=null)
                {
                    addTouchListen.OnTouchClick(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return exam3List.size();
    }
    public interface AddTouchListen{
        public void OnTouchClick(int position);

    }
}