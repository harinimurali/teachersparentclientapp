package com.school.teacherparent.fragment;


import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.school.teacherparent.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;



import static android.app.Activity.RESULT_OK;


/**
 * A simple {@link Fragment} subclass.
 */
public class AddHomeWorkFragment extends Fragment {


    private Spinner class_spinner, sub_spinner, chap_spinner, topic_spinner;
    private TextView edit_desc;
    private LinearLayout date_picker;
    private LinearLayout upload_liner;
    private View btnDatePicker;
    private int mYear, mMonth, mDay, mHour, mMinute;

    private TextView text;
    private LinearLayout lin_date;
    private static int RESULT_LOAD_IMG = 1;
    String imgDecodableString;
    private int GALLERY=1;

    private static final String IMAGE_DIRECTORY = "/demonuts";
    private ImageView img_upload;

    public AddHomeWorkFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_add_home, container, false);

        class_spinner = (Spinner)view.findViewById(R.id.class_spinner);
        sub_spinner = (Spinner) view.findViewById(R.id.sub_spinner);
        chap_spinner = (Spinner) view.findViewById(R.id.chap_spinner);
        topic_spinner = (Spinner)view. findViewById(R.id.topic_spinner);
        edit_desc = (EditText) view.findViewById(R.id.edit_desc);
        date_picker = (LinearLayout) view.findViewById(R.id.date_picker);
        lin_date = (LinearLayout)view. findViewById(R.id.lin_date);
        upload_liner = (LinearLayout)view. findViewById(R.id.upload_linear);
        img_upload=(ImageView)view.findViewById(R.id.img_upload);
        text = (TextView) view.findViewById(R.id.text);

        lin_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view == lin_date) {

                    // Get Current Date
                    final Calendar c = Calendar.getInstance();
                    mYear = c.get(Calendar.YEAR);
                    mMonth = c.get(Calendar.MONTH);
                    mDay = c.get(Calendar.DAY_OF_MONTH);


                    DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                            new DatePickerDialog.OnDateSetListener() {


                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {
                                    SimpleDateFormat curFormater = new SimpleDateFormat("dd-MM-yyyy");
                                    Date dateObj = null;
                                    String a = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                                    try {
                                        dateObj = curFormater.parse(a);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    SimpleDateFormat formt = new SimpleDateFormat("dd MMM yyyy");

                                    text.setText(formt.format(dateObj));
                                }

                            }, mYear, mMonth, mDay);
                    datePickerDialog.show();



                    String dateStr = "04/05/2010";
                    SimpleDateFormat curFormater = new SimpleDateFormat("dd/MM/yyyy");
                    Date dateObj = null;
                    try {
                        dateObj = curFormater.parse(dateStr);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    SimpleDateFormat postFormater = new SimpleDateFormat("MMMM dd, yyyy");

                    String newDateStr = postFormater.format(dateObj);


                    //upload image



                }
            }
        });
        //class spinner
        List<String> classpinner = new ArrayList<String>();
        classpinner.add("Class 5C");
        classpinner.add("Class 4C");
        classpinner.add("Class 6C");
        classpinner.add("Class 7A");
        classpinner.add("Class 8B");
        classpinner.add("Class 9C");



        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, classpinner);

        spinnerArrayAdapter.setDropDownViewResource( R.layout.spinner_item);

        class_spinner.setAdapter(spinnerArrayAdapter);

        class_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        //subject spinner
        List<String> subspinner = new ArrayList<String>();
        subspinner.add("English");
        subspinner.add("Tamil");
        subspinner.add("Maths");
        subspinner.add("Science");
        subspinner.add("Social");
        subspinner.add("Computer Science");
        sub_spinner = (Spinner) view.findViewById(R.id.sub_spinner);

        ArrayAdapter<String> subspinnerArrayadapter = new ArrayAdapter<String>(
                getContext(),  R.layout.spinner_item, subspinner);

        subspinnerArrayadapter.setDropDownViewResource( R.layout.spinner_item);

        sub_spinner.setAdapter(subspinnerArrayadapter);

        sub_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        //chapter spinner
        List<String> chapspinner = new ArrayList<String>();
        chapspinner.add("My Mother at Sixty Two");
        chapspinner.add("Shakesphere Poem");
        chapspinner.add("Punctuations");
        chapspinner.add("Articles and Pronouns");
        chapspinner.add("Nouns and Verbs");
        chapspinner.add("Active and Passive Voice");
        chap_spinner = (Spinner) view.findViewById(R.id.chap_spinner);

        ArrayAdapter<String> chapspinnerarrayadapter = new ArrayAdapter<String>(
                getContext(),  R.layout.spinner_item, chapspinner);

        chapspinnerarrayadapter.setDropDownViewResource( R.layout.spinner_item);

        chap_spinner.setAdapter(chapspinnerarrayadapter);

        chap_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        //topic spinner
        List<String> topicspinner = new ArrayList<String>();
        topicspinner.add("Topic:2.4");
        topicspinner.add("Topic:3.2");
        topicspinner.add("Topic:4.4");
        topicspinner.add("Topic:5.9");
        topicspinner.add("Topic:8.6");
        topicspinner.add("Topic:7.7");
        topic_spinner = (Spinner) view.findViewById(R.id.topic_spinner);

        ArrayAdapter<String> topicspinnerarrayadapter = new ArrayAdapter<String>(
                getContext(),  R.layout.spinner_item, topicspinner);

        topicspinnerarrayadapter.setDropDownViewResource( R.layout.spinner_item);

        topic_spinner.setAdapter(topicspinnerarrayadapter);

        topic_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        upload_liner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK,

                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                // Start the Intent

                startActivityForResult(galleryIntent, RESULT_LOAD_IMG);

                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("*/*");
                startActivityForResult(intent, 7);
            }
        });


        return view;
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), contentURI);
                    String path = saveImage(bitmap);
                    Toast.makeText(getActivity(), "Uploaded successfully", Toast.LENGTH_SHORT).show();
                    img_upload.setImageBitmap(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        }

        if (resultCode == RESULT_OK) {
            String PathHolder = data.getData().getPath();
            Toast.makeText(getActivity(), PathHolder, Toast.LENGTH_LONG).show();
        }
    }

    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(getActivity(),
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath());

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }
}
