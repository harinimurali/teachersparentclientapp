package com.school.teacherparent.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.school.teacherparent.models.ExamDTO;
import com.school.teacherparent.R;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by keerthana on 10/11/2018.
 */


public class ExamHistoryAdapter extends RecyclerView.Adapter<ExamHistoryAdapter.MyViewHolder> {

    private List<ExamDTO> examhisList=new ArrayList<>();
    private Context context;
    public AddTouchListen addTouchListen;

    public ExamHistoryAdapter(List<ExamDTO> examhisList) {
        this.examhisList = examhisList;
        this.context =context;
    }



    public void setOnClickListen(AddTouchListen addTouchListen) {
       this.addTouchListen=addTouchListen;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        private LinearLayout lin;
        private TextView title,school,counts,duration,startdate,enddate;

        private ImageView img;
        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            school=(TextView)view.findViewById(R.id.school);
            counts=(TextView)view.findViewById(R.id.counts);
            duration=(TextView)view.findViewById(R.id.duration);
            startdate=(TextView)view.findViewById(R.id.startdate);
            enddate=(TextView)view.findViewById(R.id.enddate);
            img=(ImageView)view.findViewById(R.id.img);
            lin=(LinearLayout)view.findViewById(R.id.lin);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.exam_history_exam_item, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        ExamDTO movie = examhisList.get(position);
        holder.img.setImageResource(Integer.parseInt(movie.getImg()));
        holder.title.setText(movie.getTitle());
        holder.school.setText(movie.getSchool());
        holder.counts.setText(movie.getCounts());
        holder.startdate.setText(movie.getStartdate());
        holder.enddate.setText(movie.getEnddate());
        holder.duration.setText(movie.getDuration());


        holder.lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (addTouchListen != null) {
                    addTouchListen.OnTouchClick(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return examhisList.size();
    }

    public interface AddTouchListen{
        public void OnTouchClick(int position);

    }
}