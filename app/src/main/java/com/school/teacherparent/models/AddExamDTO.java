package com.school.teacherparent.models;

/**
 * Created by keerthana on 10/8/2018.
 */


public class AddExamDTO {
    private String sub_spinner,class_spinner,date,time;


    public String getSub_spinner() {
        return sub_spinner;
    }

    public void setSub_spinner(String sub_spinner) {
        this.sub_spinner = sub_spinner;
    }

    public String getClass_spinner() {
        return class_spinner;
    }

    public void setClass_spinner(String class_spinner) {
        this.class_spinner = class_spinner;
    }


    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }




    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }







}