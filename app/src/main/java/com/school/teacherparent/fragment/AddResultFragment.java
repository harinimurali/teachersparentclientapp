package com.school.teacherparent.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.adapter.AddResultAdapter;
import com.school.teacherparent.models.AddResultDTO;
import com.school.teacherparent.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddResultFragment extends Fragment {


    private ImageView back;
    private Spinner class_spinner, sub_spinner, exam_spinner;
    private RecyclerView recyclerview;
    private Button result_btn;
    TextView add_result;
    private List<AddResultDTO> addresList = new ArrayList<>();
    private AddResultAdapter mAdapter;

    public AddResultFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_result, container, false);

        back = (ImageView) view.findViewById(R.id.back);
        SidemenuDetailActivity.title.setText(getResources().getString(R.string.add_result));
        class_spinner = (Spinner) view.findViewById(R.id.class_spinner);
        sub_spinner = (Spinner) view.findViewById(R.id.sub_spinner);
        exam_spinner = (Spinner) view.findViewById(R.id.exam_spinner);
        recyclerview = (RecyclerView) view.findViewById(R.id.recycle_add_result);
        add_result = view.findViewById(R.id.add_result);

        mAdapter = new AddResultAdapter(addresList, getActivity());

        AddResultData();

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

        recyclerview.setLayoutManager(mLayoutManager);

        recyclerview.setItemAnimator(new DefaultItemAnimator());

        recyclerview.setAdapter(mAdapter);


        add_result.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Added Successfully", Toast.LENGTH_SHORT).show();
                getActivity().finish();
                //  onBackPressed();
                /*Intent intent=new Intent(getApplicationContext(),ClassworkActivity.class);
                startActivity(intent);*/
            }
        });


        List<String> classpinner = new ArrayList<String>();
        classpinner.add("Class 5C");
        classpinner.add("Class 4C");
        classpinner.add("Class 6C");
        classpinner.add("Class 7A");
        classpinner.add("Class 8B");
        classpinner.add("Class 9C");
        class_spinner = (Spinner) view.findViewById(R.id.class_spinner);

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, classpinner);

        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);

        class_spinner.setAdapter(spinnerArrayAdapter);

        class_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });


        //subject spinner
        List<String> subspinner = new ArrayList<String>();
        subspinner.add("English");
        subspinner.add("Tamil");
        subspinner.add("Maths");
        subspinner.add("Science");
        subspinner.add("Social");
        subspinner.add("Computer Science");
        sub_spinner = (Spinner) view.findViewById(R.id.sub_spinner);

        ArrayAdapter<String> subspinnerArrayadapter = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, subspinner);

        subspinnerArrayadapter.setDropDownViewResource(R.layout.spinner_item);

        sub_spinner.setAdapter(subspinnerArrayadapter);

        sub_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        List<String> exam = new ArrayList<String>();
        exam.add("Ist Term Exam");
        exam.add("IInd Mid Term Exam");
        exam.add("Quartrely Exam");
        exam.add("Half Yearly Exam");

        exam_spinner = (Spinner) view.findViewById(R.id.exam_spinner);

        ArrayAdapter<String> examspinneradapter = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, exam);

        examspinneradapter.setDropDownViewResource(R.layout.spinner_item);

        exam_spinner.setAdapter(examspinneradapter);

        exam_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });


        return view;
    }

    private void AddResultData() {
        AddResultDTO s = new AddResultDTO();
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setName("Jaun Clevenston");
        addresList.add(s);


        s = new AddResultDTO();
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setName("Einstein");
        addresList.add(s);

        s = new AddResultDTO();
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setName("Arnorld");
        addresList.add(s);

        s = new AddResultDTO();
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setName("James");
        addresList.add(s);

        s = new AddResultDTO();
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setName("John");
        addresList.add(s);

        s = new AddResultDTO();
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setName("Clinton");
        addresList.add(s);

        s = new AddResultDTO();
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setName("Robert Clive");
        addresList.add(s);

        s = new AddResultDTO();
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setName("Lord Williams");
        addresList.add(s);

    }

}
