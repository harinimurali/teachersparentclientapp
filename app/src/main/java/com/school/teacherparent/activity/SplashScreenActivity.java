package com.school.teacherparent.activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;

import com.school.teacherparent.R;
import com.school.teacherparent.utils.AppPreferences;

/**
 * Created by harini on 8/23/2018.
 */

public class SplashScreenActivity extends AppCompatActivity implements Animation.AnimationListener {
    Button getstarted;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_splash);

        FrameLayout mainFrame = ((FrameLayout) findViewById(R.id.frame_main));
        getstarted = ((Button) findViewById(R.id.get_started));
        Animation zoom_out = AnimationUtils.loadAnimation(this,
                R.anim.zoom_out);
        zoom_out.setAnimationListener(this);
        mainFrame.startAnimation(zoom_out);
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            int verCode = pInfo.versionCode;
            //  AppPreferences.setversioncode(getApplicationContext(), String.valueOf(verCode));

            Log.e("payment", "version" + version + "-" + verCode);
        } catch (PackageManager.NameNotFoundException e) {

            e.printStackTrace();
        }
        getstarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(SplashScreenActivity.this, LoginActivity.class);
                // }
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);


                startActivity(i);
                finish();

            }
        });
       /* UpdateMe.with(this, 30).setDialogVisibility(true)
                .continueButtonVisibility(true)
                .setPositiveButtonText("Update")
                //.setNegativeButtonText("Oh No...")
                .setDialogIcon(R.drawable.logo_copy)
                .onNegativeButtonClick(new OnNegativeButtonClickListener() {

                    @Override
                    public void onClick(LovelyStandardDialog dialog) {

                        Log.d(UpdateMe.TAG, "Later Button Clicked");
                        dialog.dismiss();
                    }
                })
                .onPositiveButtonClick(new OnPositiveButtonClickListener() {

                    @Override
                    public void onClick(LovelyStandardDialog dialog) {

                        Log.d(UpdateMe.TAG, "Update Button Clicked");
                        dialog.dismiss();
                    }
                })
                .check();*/

    }

    @Override
    public void onAnimationStart(Animation animation) {
    }

    @Override
    public void onAnimationEnd(Animation animation) {

       /* Intent i;
        if (AppPreferences.isLoggedIn(SplashScreenActivity.this)) {
            i = new Intent(SplashScreenActivity.this, MainActivity.class);

        } else {*/

/*
        if (Utils.isConnected(mCurrentActivity) &&
                !AppPreferences.getUserProfile(mCurrentActivity).getAccessToken().equalsIgnoreCase("")) {
            new UserRepository().accessTokenValidation(this, iUserDataHandler);
            new UserRepository().requestAppSettings(mCurrentActivity, iUserDataHandler);
        } else if (!Utils.isConnected(mCurrentActivity) &&
                !AppPreferences.getUserProfile(mCurrentActivity).getAccessToken().equalsIgnoreCase(""))
            iUserDataHandler.onTokenValidated();
        else
            iUserDataHandler.onError("");*/
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
