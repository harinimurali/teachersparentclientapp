package com.school.teacherparent.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.school.teacherparent.models.ClapListDTO;
import com.school.teacherparent.R;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by keerthana on 10/18/2018.
 */

public class ClapAdapter extends RecyclerView.Adapter<ClapAdapter.MyViewHolder> {

    private List<ClapListDTO> clapList=new ArrayList<>();
    private Context context;
    private AddTouchListen addTouchListen;


    public ClapAdapter(List<ClapListDTO> clapList) {

        this.clapList=clapList;
    }



    public class MyViewHolder extends RecyclerView.ViewHolder {


        private ImageView stu_img;
        private TextView stu_name;

        public MyViewHolder(View view) {
            super(view);
            stu_img=view.findViewById(R.id.stu_img);
            stu_name=view.findViewById(R.id.stu_name);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.claps_recycle_item, parent, false);

        return new MyViewHolder(itemView);
    }
    public void setOnClickListen(AddTouchListen addTouchListen) {
        this.addTouchListen = addTouchListen;
    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        ClapListDTO movie = clapList.get(position);
        holder.stu_name.setText(movie.getStu_name());
        holder.stu_img.setImageResource(Integer.parseInt(movie.getStu_img()));





    }

    @Override
    public int getItemCount() {
        return clapList.size();
    }

    public interface AddTouchListen {
        public void onTouchClick(int position);
    }
}