package com.school.teacherparent.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.school.teacherparent.models.HomeDTO;
import com.school.teacherparent.R;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by keerthana on 9/27/2018.
 */

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.MyViewHolder> {

    private ImageView img;
    private TextView name,std,school;
    private List<HomeDTO> homeList=new ArrayList<>();
    private Context context;
    public AddTouchListen addTouchListen;

    public HomeAdapter(List<HomeDTO> homeList) {
        this.homeList = homeList;
        this.context =context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        private  LinearLayout lin;
        private  TextView subname,classname,title1,title2,subtitle1,subtitle2,date1,date2;
        private  ImageView img1,img2,img3;
        public TextView counts;


        public MyViewHolder(View view) {
            super(view);


            counts = (TextView) view.findViewById(R.id.counts);
            img1=(ImageView)view.findViewById(R.id.img1);
            img2=(ImageView)view.findViewById(R.id.img2);
            img3=(ImageView)view.findViewById(R.id.img3);
            subname = (TextView) view.findViewById(R.id.subname);
            classname = (TextView) view.findViewById(R.id.classname);
            title1 = (TextView) view.findViewById(R.id.title1);
            title2 = (TextView) view.findViewById(R.id.title2);
            date1 = (TextView) view.findViewById(R.id.date1);
            date2 = (TextView) view.findViewById(R.id.date2);
            subtitle1 = (TextView) view.findViewById(R.id.subtitle1);
            subtitle2 = (TextView) view.findViewById(R.id.subtitle2);
            lin=(LinearLayout)view.findViewById(R.id.lin);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_item, parent, false);

        return new MyViewHolder(itemView);
    }
    public void setOnClickListen(AddTouchListen addTouchListen) {
        this.addTouchListen = addTouchListen;
    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        HomeDTO movie = homeList.get(position);
        holder.counts.setText(movie.getCounts());
        holder.subtitle1.setText(movie.getSubtitle());
        holder.subtitle2.setText(movie.getSubtitle2());
        holder.title1.setText(movie.getTitle());
        holder.title2.setText(movie.getTitle2());
        holder.date1.setText(movie.getDate1());
        holder.date2.setText(movie.getDate2());
        holder.classname.setText(movie.getClassname());
        holder.subname.setText(movie.getSubname());
       /* holder.img1.setImageResource(Integer.parseInt(movie.getImg1()));
        holder.img2.setImageResource(Integer.parseInt(movie.getImg2()));
        holder.img3.setImageResource(Integer.parseInt(movie.getImg3()));*/


        holder.lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (addTouchListen != null) {
                    addTouchListen.onTouchClick(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return homeList .size();
    }
    public interface AddTouchListen {
        public void onTouchClick(int position);
    }
}