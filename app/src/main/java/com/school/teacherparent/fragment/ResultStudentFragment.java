package com.school.teacherparent.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.adapter.ResultStudentAdapter;
import com.school.teacherparent.models.ResultStudentDTO;
import com.school.teacherparent.R;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class ResultStudentFragment extends Fragment {


    private RecyclerView recyclerView;
    private ResultStudentAdapter mAdapter;
    private List<ResultStudentDTO> resstuList = new ArrayList<>();
    private LinearLayout lin;
    private TextView title;
    private String[] myString;
    ImageView noti, back;

    public ResultStudentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_result_student, container, false);


        recyclerView = (RecyclerView) view.findViewById(R.id.recycle_result_stuname);
        lin = (LinearLayout) view.findViewById(R.id.lin);
        recyclerView.setNestedScrollingEnabled(false);
        noti = view.findViewById(R.id.noti);
        back = view.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new ResultClassListFragment();
                replaceFragment(fragment);
            }
        });
        noti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), NotificationActivity.class);
                startActivity(intent);
            }
        });

        myString = new String[4];
        for (int i = 0; i < myString.length; i++) {
            view.setLayoutParams(
                    new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT));

            view.getParent();
            Resultdata();

            mAdapter = new ResultStudentAdapter(resstuList, getActivity());
            recyclerView.setAdapter(mAdapter);
        }


        mAdapter = new ResultStudentAdapter(resstuList, getActivity());
        //   recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(mAdapter);


       /* mAdapter.setOnClickListen(new ResultStudentAdapter.AddTouchListen() {
            @Override
            public void OnTouchClick(int position) {
                startActivity(new Intent(getActivity(), SidemenuDetailActivity.class).putExtra("type", "result"));
            }
        });
*/

        return view;
    }


    private void replaceFragment(Fragment fragment) {
        String backStateName;
        backStateName = ((Object) fragment).getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.result_frame, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);//
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }


    private void Resultdata() {
        ResultStudentDTO s = new ResultStudentDTO();
        s.setSubject("English");
        s.setMarks1("100");
        s.setStmarks("87/");
        s.setImg(String.valueOf(R.mipmap.english_icon));
        resstuList.add(s);


        s = new ResultStudentDTO();

        s.setSubject("Maths");
        s.setMarks1("100");
        s.setStmarks("90/");
        s.setImg(String.valueOf(R.mipmap.maths_icon));
        resstuList.add(s);

        s = new ResultStudentDTO();

        s.setSubject("Science");
        s.setMarks1("100");
        s.setStmarks("77/");
        s.setImg(String.valueOf(R.mipmap.english_icon));
        resstuList.add(s);

        s = new ResultStudentDTO();

        s.setSubject("Social");
        s.setMarks1("100");
        s.setStmarks("80/");
        s.setImg(String.valueOf(R.mipmap.maths_icon));
        resstuList.add(s);


        s = new ResultStudentDTO();
        s.setSubject("Computer Science");
        s.setMarks1("100");
        s.setStmarks("70/");
        s.setImg(String.valueOf(R.mipmap.english_icon));
        resstuList.add(s);


    }

}
