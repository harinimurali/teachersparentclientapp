package com.school.teacherparent.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.school.teacherparent.models.ClassDTO;
import com.school.teacherparent.R;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by keerthana on 9/20/2018.
 */

public class ClassAdapter extends RecyclerView.Adapter<ClassAdapter.MyViewHolder> {

    private List<ClassDTO> classList=new ArrayList<>();
    private Context context;
    private AddTouchListen addTouchListen;
    private ProgressBar prg1;
    private ProgressBar Prg1;
    private ProgressBar prg2;

    public ClassAdapter(List<ClassDTO> classList) {

        this.classList=classList;
    }

    /*public ClassAdapter(List<ClassDTO> classList) {
        this.classList = classList;

    }*/

    public class MyViewHolder extends RecyclerView.ViewHolder {

       // private ProgressBar prg1,prg2;
        private  TextView counts;
        private  ImageView img,img2;
        private  TextView title,content,status1,status2;
        LinearLayout header;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title1);
            content = (TextView) view.findViewById(R.id.content1);
            img=(ImageView)view.findViewById(R.id.img);
            img2=(ImageView)view.findViewById(R.id.img2);
            status1=(TextView)view.findViewById(R.id.status1);
            status2=(TextView)view.findViewById(R.id.status2);
            counts=(TextView)view.findViewById(R.id.counts);
            header=view.findViewById(R.id.header);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.class_item, parent, false);

        return new MyViewHolder(itemView);
    }
    public void setOnClickListen(AddTouchListen addTouchListen) {
        this.addTouchListen = addTouchListen;
    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        ClassDTO movie = classList.get(position);
        holder.title.setText(movie.getTitle());
        holder.content.setText(movie.getContent());
        holder.img.setImageResource(Integer.parseInt(movie.getImg()));
        holder.img2.setImageResource(Integer.parseInt(movie.getImg2()));
        holder.status2.setText(movie.getStatus2());
        holder.status1.setText(movie.getStatus1());
        holder.counts.setText(movie.getCounts());


        holder.header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (addTouchListen != null) {
                    addTouchListen.onTouchClick(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return classList.size();
    }

    public interface AddTouchListen {
        public void onTouchClick(int position);
    }
}