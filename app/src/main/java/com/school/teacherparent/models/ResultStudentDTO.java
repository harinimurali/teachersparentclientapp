package com.school.teacherparent.models;

/**
 * Created by keerthana on 10/4/2018.
 */


public class ResultStudentDTO {


    private String img, title,subject,stmarks,marks1;



    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }


    public String getStmarks() {
        return stmarks;
    }

    public void setStmarks(String stmarks) {
        this.stmarks = stmarks;
    }

    public String getMarks1() {
        return marks1;
    }

    public void setMarks1(String marks1) {
        this.marks1 = marks1;
    }


}
