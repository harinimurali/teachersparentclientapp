package com.school.teacherparent.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.school.teacherparent.models.AddExamDTO;
import com.school.teacherparent.R;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by keerthana on 10/8/2018.
 */

public class AddExamAdapter extends RecyclerView.Adapter<AddExamAdapter.MyViewHolder> {

    private List<AddExamDTO> addexamList=new ArrayList<>();
    private Context context;
    public AddTouchListen addTouchListen;
    private Spinner class_spinner;
    private Spinner sub_spinner;


    public AddExamAdapter(List<AddExamDTO> addexamList, Context context) {
        super();
        this.addexamList=addexamList;
        this.context=context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private  RelativeLayout relative_class_spinner,relative_sub_spinner;
        public TextView title, subtitle, time;
        ImageView img;
        LinearLayout lin;
        public TextView desc,subdesc,date;
        public TextView content;
        private Spinner sub_spinner,class_spinner;

        public MyViewHolder(View view) {
            super(view);
            date = (TextView) view.findViewById(R.id.date);
            time = (TextView) view.findViewById(R.id.time);
            sub_spinner=(Spinner)view.findViewById(R.id.sub_spinner);
            class_spinner=(Spinner)view.findViewById(R.id.class_spinner);

        }
    }



    public AddExamAdapter(List<AddExamDTO> addexamList) {

        this.addexamList = addexamList;


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.add_exam_recycle_item, parent, false);


        List<String> classpinner1 = new ArrayList<String>();
        classpinner1.add("2 Classs");
        classpinner1.add("3 Classs");
        classpinner1.add("4 Classs");
        classpinner1.add("5 Classs");


        class_spinner=(Spinner)itemView.findViewById(R.id.class_spinner);
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                itemView.getContext(), R.layout.spinner_item, classpinner1);

        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);

        class_spinner.setAdapter(spinnerArrayAdapter);

        class_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        List<String> sub = new ArrayList<String>();
        sub.add("English");
        sub.add("Maths");
        sub.add("Science");
        sub.add("Social");
        sub.add("Computer Science");

        sub_spinner=(Spinner)itemView.findViewById(R.id.sub_spinner);
        ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<String>(
                itemView.getContext(), R.layout.spinner_item, sub);

        spinnerArrayAdapter1.setDropDownViewResource(R.layout.spinner_item);

        sub_spinner.setAdapter(spinnerArrayAdapter1);

        sub_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        return new MyViewHolder(itemView);
    }

    public void OnsetClickListen(AddTouchListen addTouchListen)
    {
        this.addTouchListen=addTouchListen;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        AddExamDTO movie = addexamList.get(position);
        holder.date.setText(movie.getDate());
        holder.time.setText(movie.getTime());


    }

    @Override
    public int getItemCount() {
        return addexamList.size();
    }

    public interface AddTouchListen{
         void OnTouchClick(int position);

    }
}
