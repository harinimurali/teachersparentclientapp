package com.school.teacherparent.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;

import com.school.teacherparent.activity.NewMessageActivity;
import com.school.teacherparent.activity.TestDrawerActivity;
import com.school.teacherparent.adapter.MessageAdapter;
import com.school.teacherparent.models.MessageDTO;
import com.school.teacherparent.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by harini on 8/29/2018.
 */

public class MessageFragment extends Fragment {
    RecyclerView message_recycle;
    MessageAdapter messageAdapter;
    List<MessageDTO> message;
    FloatingActionButton newMessage;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_message, container, false);
        TestDrawerActivity.toolbar.setTitle("Message");


        message_recycle = view.findViewById(R.id.message_recycle);
        newMessage = view.findViewById(R.id.new_msg);

        newMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), NewMessageActivity.class));
            }
        });
        message = new ArrayList<>();
        AnalData();
        messageAdapter = new MessageAdapter(message, getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        message_recycle.setLayoutManager(mLayoutManager);
        message_recycle.setItemAnimator(new DefaultItemAnimator());
        message_recycle.setAdapter(messageAdapter);
        setHasOptionsMenu(true);

        return view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        getActivity().getMenuInflater().inflate(R.menu.main, menu);
        menu.getItem(0).setIcon(ContextCompat.getDrawable(getActivity(), R.mipmap.search_icon));
        super.onPrepareOptionsMenu(menu);
    }

    private void AnalData() {

        MessageDTO s = new MessageDTO();
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setTitle("Preetha");
        s.setContent("Thanks again, Have a Wonderful day");
        s.setTime("10/11/2018");

        message.add(s);


        s = new MessageDTO();
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setTitle("Rahul");
        s.setContent("Have a Great day to meet you");
        s.setTime("08/09/18");
        message.add(s);


        s = new MessageDTO();
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setTitle("Caroline");
        s.setTime("9/11/2018");
        s.setContent("Right now I am busy I call you later");
        message.add(s);


        s = new MessageDTO();
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setTitle("Daniel");
        s.setContent("I am not feeling well");
        s.setTime("10/11/2018");
        message.add(s);


        s = new MessageDTO();
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setTitle("Shanthanu");
        s.setContent("Call me Later, Now IAm in meeting");
        s.setTime("05/11/2018");
        message.add(s);

        s = new MessageDTO();
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setTitle("Daniel");
        s.setContent("I am not feeling well");
        s.setTime("10/11/2018");
        message.add(s);


        s = new MessageDTO();
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setTitle("Shanthanu");
        s.setContent("Call me Later, Now IAm in meeting");
        s.setTime("05/11/2018");
        message.add(s);

        s = new MessageDTO();
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setTitle("Daniel");
        s.setContent("I am not feeling well");
        s.setTime("10/11/2018");
        message.add(s);


        s = new MessageDTO();
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setTitle("Shanthanu");
        s.setContent("Call me Later, Now IAm in meeting");
        s.setTime("05/11/2018");
        message.add(s);
    }
}

