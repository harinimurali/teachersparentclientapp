package com.school.teacherparent.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.school.teacherparent.R;

import java.util.List;

/**
 * Created by harini on 8/27/2018.
 */

public class FeedsAdapter extends RecyclerView.Adapter<FeedsAdapter.ViewHolder> {

    private List<String> showing;
    private Context context;
    AddTouchListen addTouchListen;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView filmname, category, status, percentage, views, clapsCount, commentsCount, shareCount;
        public ImageView image_now_showing, settings;
        LinearLayout share_lay;


        public ViewHolder(View view) {
            super(view);
            share_lay = view.findViewById(R.id.share_lay);
            clapsCount = view.findViewById(R.id.claps_count);
            commentsCount = view.findViewById(R.id.comments_count);
            shareCount = view.findViewById(R.id.share_count);
           /* filmname = (TextView) view.findViewById(R.id.text_label_now_showing_film_name);
            category = (TextView) view.findViewById(R.id.text_label_film_category);
            status = (TextView) view.findViewById(R.id.text_label_status);
            percentage = (TextView) view.findViewById(R.id.text_label_now_showing_percentage);
            views = (TextView) view.findViewById(R.id.text_label_now_showing_views);
            image_now_showing = view.findViewById(R.id.image_now_showing);
*/
            settings = view.findViewById(R.id.report);
        }
    }

    public void setOnClickListen(AddTouchListen addTouchListen) {
        this.addTouchListen = addTouchListen;
    }

    public interface AddTouchListen {
        //public void onIncrement(int position, TextView integer, TextView displayInteger);
        //  public void onDecrement(int position, TextView integer, TextView displayInteger);
        void onShareClick(int position);

        void onTouchClick(int position);

        void onCommentClick(int position);

        void onClapsClick(int position);

        void onClapsCountClick(int position);

    }


    public FeedsAdapter(List<String> showing, Context context) {
        this.showing = showing;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.feeds_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
       /* NowShowing_cl.Result now = showing.get(position);
        holder.filmname.setText(now.getMovie_original_title());
        holder.category.setText(now.getMovie_language());
        holder.status.setText(now.getMovie_certificate().getMovie_certificate());
        holder.percentage.setText(now.getMovie_average() + " %");
        holder.views.setText(now.getMovie_votes() + " views");
        Picasso.with(context).
                load(context.getString(R.string.coming_soon_image_base_url)
                        + now.getMovie_backgroundimg()).placeholder(R.drawable.default_movie).
                into(holder.image_now_showing);
        holder.image_now_showing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addTouchListen.onTouchClick(position);
            }
        });*/
        holder.settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addTouchListen.onTouchClick(position);
            }
        });
        holder.share_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addTouchListen.onShareClick(position);
            }
        });
        holder.clapsCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addTouchListen.onClapsCountClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        //return showing.size();
        return 4;
    }


}

