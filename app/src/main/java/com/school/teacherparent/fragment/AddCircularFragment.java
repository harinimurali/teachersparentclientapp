package com.school.teacherparent.fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static android.app.Activity.RESULT_OK;


public class AddCircularFragment extends Fragment {

    private EditText edit, editdesc;
    private Spinner spinner, spinner_class;
    private CardView upload_linear;
    private RadioButton radio;
    private RadioGroup radiogroup;
    private String radioButton = "1";
    private ImageView img_upload;
    private static int RESULT_LOAD_IMG = 1;
    String imgDecodableString;
    private int GALLERY = 1;
    ImageView back;
    TextView add_circular;
    private static final String IMAGE_DIRECTORY = "/demonuts";
    private CheckBox check;

    public AddCircularFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_circular, container, false);


        check = view.findViewById(R.id.check);
        img_upload = view.findViewById(R.id.img_upload);
        add_circular = view.findViewById(R.id.add_circular);

        SidemenuDetailActivity.title.setText(getResources().getString(R.string.add_new_circular));
        edit = view.findViewById(R.id.edit_title);

        editdesc = view.findViewById(R.id.edit_desc);

        upload_linear = view.findViewById(R.id.upload_linear);
        /*back = view.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });*/
        add_circular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        check.setOnClickListener(new View.OnClickListener() {
            int position;
            int selectedPosition = 1;

            @Override
            public void onClick(View view) {

                if (position == selectedPosition) {
                    check.setChecked(false);
                    selectedPosition = -1;
                } else {
                    selectedPosition = position;

                }
            }
        });

        //spinner class for send circular
        List<String> categories = new ArrayList<String>();
        categories.add("Jaun Keich");
        categories.add("Stephen");
        categories.add("Caroline");
        categories.add("Daniel");
        categories.add("John");
        categories.add("Michael");
        spinner = view.findViewById(R.id.spinner);

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, categories);

        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);

        spinner.setAdapter(spinnerArrayAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });


        //spinner class for classpinner
        List<String> classpinner = new ArrayList<String>();
        classpinner.add("Class 5C");
        classpinner.add("Class 4C");
        classpinner.add("Class 6C");
        classpinner.add("Class 7A");
        classpinner.add("Class 8B");
        classpinner.add("Class 9C");
        spinner_class = view.findViewById(R.id.spinner_class);

        ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, classpinner);

        spinnerArrayAdapter1.setDropDownViewResource(R.layout.spinner_item);

        spinner_class.setAdapter(spinnerArrayAdapter1);

        spinner_class.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });
        upload_linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK,

                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                // Start the Intent

                startActivityForResult(galleryIntent, RESULT_LOAD_IMG);

                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("*/*");
                startActivityForResult(intent, 7);
            }
        });
        return view;
    }

    /* public void loadImagefromGallery(View view) {

         // Create intent to Open Image applications like Gallery, Google Photos

         Intent galleryIntent = new Intent(Intent.ACTION_PICK,

                 android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

         // Start the Intent

         startActivityForResult(galleryIntent, RESULT_LOAD_IMG);

     }*/
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), contentURI);
                    String path = saveImage(bitmap);
                    Toast.makeText(getActivity(), "Uploaded successfully", Toast.LENGTH_SHORT).show();
                    img_upload.setImageBitmap(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                    //   Toast.makeText(MainActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        }

        if (resultCode == RESULT_OK) {
            String PathHolder = data.getData().getPath();
            Toast.makeText(getActivity(), PathHolder, Toast.LENGTH_LONG).show();
        }
    }

    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(getActivity(),
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath());

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }


}


























