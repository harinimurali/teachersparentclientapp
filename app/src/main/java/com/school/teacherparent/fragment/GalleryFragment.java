package com.school.teacherparent.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.R;

/**
 * Created by harini on 10/12/2018.
 */

public class GalleryFragment extends Fragment {
    public GalleryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_gallery, container, false);
        SidemenuDetailActivity.title.setText(getResources().getString(R.string.gallery));
        return view;
    }
}
