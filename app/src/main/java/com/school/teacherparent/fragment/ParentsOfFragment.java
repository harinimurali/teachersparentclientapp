package com.school.teacherparent.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.school.teacherparent.adapter.ParentsOfAdapter;
import com.school.teacherparent.models.ParentsOfDTO;
import com.school.teacherparent.R;

import java.util.ArrayList;
import java.util.List;



/**
 * A simple {@link Fragment} subclass.
 */
public class ParentsOfFragment extends Fragment {


    private RecyclerView recyclerview;
    private ParentsOfAdapter mAdapter;
    private List<ParentsOfDTO> parentsList=new ArrayList<>();

    public ParentsOfFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

       View view=inflater.inflate(R.layout.fragment_parents_of, container, false);

       recyclerview=(RecyclerView)view.findViewById(R.id.parentsof_recycle);

        mAdapter = new ParentsOfAdapter(parentsList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerview.setLayoutManager(mLayoutManager);
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        recyclerview.setAdapter(mAdapter);
        Analdata();


        return view;
    }

    private void Analdata() {


        ParentsOfDTO s=new ParentsOfDTO();
        s.setTitle("Jauun Kiech");
        s.setImg(String.valueOf(R.mipmap.student_image));
       s.setContent("Class 5C");
       s.setSchool("Burnley School");
        parentsList.add(s);


        s=new ParentsOfDTO();
        s.setTitle("Caroline");
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setContent("Class 5A");
        s.setSchool("Burnley School");
        parentsList.add(s);


        s=new ParentsOfDTO();
        s.setTitle("Mary Schuma");
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setContent("Class 6C");
        s.setSchool("Burnley School");
        parentsList.add(s);


        s=new ParentsOfDTO();
        s.setTitle("Albert Einstein");
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setContent("Class 7C");
        s.setSchool("Burnley School");
        parentsList.add(s);

       /* s=new ParentsOfDTO();
        s.setTitle("Stephen");
        s.setImg(String.valueOf(R.drawable.dp));
        s.setContent("Class 9C");
        s.setSchool("Burnley School");
        parentsList.add(s);*/

    }

}
